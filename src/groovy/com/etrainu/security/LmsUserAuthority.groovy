package com.etrainu.security

import org.springframework.security.core.GrantedAuthority

class LmsUserAuthority implements GrantedAuthority {
	
	String authority
	
	LmsUserAuthority(){}
	
	LmsUserAuthority(String authority) {
		this.authority = authority
	}
	
	public String getAuthority() {
		return this.authority
	}
}
