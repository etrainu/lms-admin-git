package com.etrainu.security

import java.io.IOException

import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.codehaus.groovy.grails.plugins.springsecurity.AjaxAwareAuthenticationSuccessHandler
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import org.springframework.security.core.Authentication

class AuthenticationSuccessHandler extends AjaxAwareAuthenticationSuccessHandler {

	@Override
	public void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response, final Authentication authentication) throws ServletException, IOException {
		/*
		 * This action will redirect the user to what ever action should be their default
		 * Its to prevent things like having a different login page for every user
		 * Such as calculating and setting up redirect params on the login page which may cause issues
		 * 
		 * TODO: Figure out if i can access createLink to do proper linking using grails, rather than hardcoding contextPath
		 */
		response.sendRedirect( request.contextPath + getUserEndpoint() )
	}
	
	final String getUserEndpoint() {
		if( SpringSecurityUtils.ifAnyGranted('ROLE_VIEW_USER') ) {
			return '/user/user'
		}

		if( SpringSecurityUtils.ifAnyGranted('ROLE_VIEW_COURSE') ) {
			return '/course/search'
		}

		if( SpringSecurityUtils.ifAnyGranted('ROLE_VIEW_REFERRER') ) {
			return '/referrer/search'
		}

		if( SpringSecurityUtils.ifAnyGranted('ROLE_VIEW_SALES_FIGURES_REPORT') ) {
			return '/sales/report'
		}
		
		//This is the default case, no need for complex retrieval of user as the spring security service will reject
		//Public users to a login page
		return "/user/user"
	}
}
