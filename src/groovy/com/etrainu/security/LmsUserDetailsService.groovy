package com.etrainu.security

import org.apache.log4j.Logger
import org.codehaus.groovy.grails.plugins.springsecurity.GrailsUserDetailsService
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import org.springframework.security.core.authority.GrantedAuthorityImpl
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException

import com.etrainu.user.User

class LmsUserDetailsService implements GrailsUserDetailsService {
	
	def userService
	private static def log = Logger.getLogger(getClass())
	
	static final List NO_ROLES = [
		new GrantedAuthorityImpl(SpringSecurityUtils.NO_ROLE)
	]

	UserDetails loadUserByUsername(String username, boolean loadRoles) throws UsernameNotFoundException {
		return loadUserByUsername(username)
	}

	UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user
		
		try {
			user = userService.getUserForAuthentication(username)
		} catch(Throwable e) {
			log.error e.getMessage()
			throw new UsernameNotFoundException('User not found', username)
		}
			
		 if (!user || !user.permissions || user.permissions.length() == 0) throw new UsernameNotFoundException('User not found', username)
		 
		 def authorities = user.permissions.split(/,/).collect {
			 new LmsUserAuthority(it)
		 }
		 
		 def userDetails = new LmsUserDetails(user.username, user.password, user.active, (user.archiveDate == null), (user.archiveDate == null), user.active, authorities ?: NO_ROLES, user.id)
		 
		 //Since the userDetails isn't a bean, just a regular class, we need to inject the dependancy manually
		 userDetails.userService = userService
		 
		 return userDetails
	}
}