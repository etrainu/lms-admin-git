package com.etrainu.security

import org.codehaus.groovy.grails.plugins.springsecurity.GrailsUser
import org.springframework.security.core.GrantedAuthority

import com.etrainu.shared.service.UserService

class LmsUserDetails extends GrailsUser {
	
	def TOKEN_NOT_OBTAINED = 1
	def TOKEN_OBTAINING = 2
	def TOKEN_OBTAINED = 3

	//def passwordEncoder = new ShaPasswordEncoder(256)
	
	def userService
	
	String apiToken;
	int apiTokenState = this.TOKEN_NOT_OBTAINED;
	

	LmsUserDetails(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<GrantedAuthority> authorities, String id) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities, id)
	}
	
	// do NOT get the api token on initalize of the user because they may not have entered a valid password
	// and might not be granted the access
	public String getApiToken() {
		if( !apiToken && !(this.apiTokenState == this.TOKEN_OBTAINING) ) { //Ensure its not currently retrieving the token
			this.apiTokenState = this.TOKEN_OBTAINING;
			this.apiToken = userService.authenticateUser(this.username, this.password)
			this.apiTokenState = this.TOKEN_OBTAINED;
		}
		return this.apiToken
	}
}