import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

class BootStrap {

	def springSecurityService

	def init = { servletContext ->
		//ALL roles must start with ROLE_ to be used with the secured annotation
		//def viewUserRole = SecRole.findByAuthority("ROLE_VIEW_USER") ?: new SecRole(authority:"ROLE_VIEW_USER").save(failOnError:true)
		//def editUserRole = SecRole.findByAuthority("ROLE_EDIT_USER") ?: new SecRole(authority:"ROLE_EDIT_USER").save(failOnError:true)
		//def archiveUserRole = SecRole.findByAuthority("ROLE_ARCHIVE_USER") ?: new SecRole(authority:"ROLE_ARCHIVE_USER").save(failOnError:true)

		//def viewCourseRole = SecRole.findByAuthority("ROLE_VIEW_COURSE") ?: new SecRole(authority:"ROLE_VIEW_COURSE").save(failOnError:true)
		//def editCourseRole = SecRole.findByAuthority("ROLE_EDIT_COURSE") ?: new SecRole(authority:"ROLE_EDIT_COURSE").save(failOnError:true)
		//def archiveCourseRole = SecRole.findByAuthority("ROLE_ARCHIVE_COURSE") ?: new SecRole(authority:"ROLE_ARCHIVE_COURSE").save(failOnError:true)

		//def viewReferrerRole = SecRole.findByAuthority("ROLE_VIEW_REFERRER") ?: new SecRole(authority:"ROLE_VIEW_REFERRER").save(failOnError:true)
		//def editReferrerRole = SecRole.findByAuthority("ROLE_EDIT_REFERRER") ?: new SecRole(authority:"ROLE_EDIT_REFERRER").save(failOnError:true)
		//def archiveReferrerRole = SecRole.findByAuthority("ROLE_ARCHIVE_REFERRER") ?: new SecRole(authority:"ROLE_ARCHIVE_REFERRER").save(failOnError:true)
		
		//def salesFiguresReportRole = SecRole.findByAuthority("ROLE_VIEW_SALES_FIGURES_REPORT") ?: new SecRole(authority:"ROLE_VIEW_SALES_FIGURES_REPORT").save(failOnError:true)

		//Admin Users
		/*[
			new AppUser(username: "steele.parker", password: springSecurityService.encodePassword("#%GYY&8"), enabled: true).save() ,
			new AppUser(username: "tobias.robinson", password: springSecurityService.encodePassword("##%544"), enabled: true).save() ,
			new AppUser(username: "darren.fresta", password: springSecurityService.encodePassword("#567*#&"), enabled: true).save() ,
			new AppUser(username: "yoann.trouillet", password: springSecurityService.encodePassword("962#@4"), enabled: true).save() ,
			new AppUser(username: "sue.lowe", password: springSecurityService.encodePassword("*@353244%"), enabled: true).save(),
			new AppUser(username: "natalie.scopelliti", password: springSecurityService.encodePassword("649#@25&"), enabled: true).save()
		].each { usr ->
			[
				viewUserRole,
				editUserRole,
				archiveUserRole,
				viewCourseRole,
				editCourseRole,
				archiveCourseRole,
				viewReferrerRole,
				editReferrerRole,
				archiveReferrerRole,
				salesFiguresReportRole
			].each { role ->
				SecUserSecRole.create usr, role
			}
		}

		//RTO Users
		[
			new AppUser(username: "robin.thompson", password: springSecurityService.encodePassword("#%2&834"), enabled: true).save() ,
			new AppUser(username: "nicole.graham", password: springSecurityService.encodePassword("#&324&*3"), enabled: true).save()
		].each { usr ->
			[
				viewUserRole,
				editUserRole,
				archiveUserRole,
				viewCourseRole
			].each { role ->
				SecUserSecRole.create usr, role
			}
		}

		//Sales
		[
			new AppUser(username: "mark.rockley", password: springSecurityService.encodePassword("^235#@5"), enabled: true).save() ,
			new AppUser(username: "paul.hoon", password: springSecurityService.encodePassword("!52357%^"), enabled: true).save() ,
			new AppUser(username: "glenda.smith", password: springSecurityService.encodePassword("&234&@%"), enabled: true).save() ,
			new AppUser(username: "luke.phelan", password: springSecurityService.encodePassword("%#2436^&"), enabled: true).save() ,
			new AppUser(username: "elsa.wright", password: springSecurityService.encodePassword("^679*#&"), enabled: true).save() ,
			new AppUser(username: "carol.bermingham", password: springSecurityService.encodePassword("@%^346#"), enabled: true).save() ,
			new AppUser(username: "jamie.fitzpatrick", password: springSecurityService.encodePassword("#@&555"), enabled: true).save()
		].each { usr ->
			[
				viewUserRole,
				viewCourseRole,
				viewReferrerRole,
				editReferrerRole,
				archiveReferrerRole,
				salesFiguresReportRole
			].each { role ->
				SecUserSecRole.create usr, role
			}
		}

		//HelpDesk
		def laurendonaldson = new AppUser(username: "lauren.donaldson", password: springSecurityService.encodePassword("@#%634"), enabled: true).save()
		[
			viewUserRole,
			editUserRole,
			archiveUserRole,
			viewCourseRole,
			viewReferrerRole
		].each { role ->
			SecUserSecRole.create laurendonaldson, role
		}

		//PM
		[
			new AppUser(username: "jaimee.blackler", password: springSecurityService.encodePassword("&%#777"), enabled: true).save() ,
			new AppUser(username: "avril.banning", password: springSecurityService.encodePassword("2%&734"), enabled: true).save()
		].each { usr ->
			[
				viewUserRole,
				editUserRole,
				archiveUserRole,
				viewCourseRole,
				editCourseRole,
				archiveCourseRole,
				viewReferrerRole,
				editReferrerRole,
				archiveReferrerRole
			].each { role ->
				SecUserSecRole.create usr, role
			}
		}

		//Assessors
		//+mark.rockley - Already has all these permissions
		[
			new AppUser(username: "kim.middlemiss", password: springSecurityService.encodePassword("*5672^%5"), enabled: true).save() ,
			new AppUser(username: "assessor", password: springSecurityService.encodePassword("^3467#%#"), enabled: true).save()
		].each { usr ->
			[
				viewUserRole,
				viewCourseRole
			].each { role ->
				SecUserSecRole.create usr, role
			}
		}

		//Dev Team
		[
			new AppUser(username: "olivia.hall", password: springSecurityService.encodePassword("424%@#"), enabled: true).save() ,
			new AppUser(username: "nicole.brown", password: springSecurityService.encodePassword("%238%@"), enabled: true).save() ,
			new AppUser(username: "rebecca.broom", password: springSecurityService.encodePassword("@#255^&"), enabled: true).save() ,
			new AppUser(username: "rob.stewart", password: springSecurityService.encodePassword("*45625^&"), enabled: true).save()
		].each { usr ->
			SecUserSecRole.create usr, viewCourseRole
		}*/
		
		//show the db
		if( CH.config.launchSQLDebug ) {
			org.hsqldb.util.DatabaseManager.main()
		}
	}
	def destroy = {

	}
}
