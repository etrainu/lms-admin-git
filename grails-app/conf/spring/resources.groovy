import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils

import com.etrainu.security.AuthenticationSuccessHandler

// Place your Spring DSL code here
beans = {
	userDetailsService(com.etrainu.security.LmsUserDetailsService) {bean ->
         bean.autowire = "byName"
   }
   saltSource(org.springframework.security.authentication.dao.SystemWideSaltSource) {
      systemWideSalt = 'B1#hb6)omIIQUltgQs#iBOA@NSQ8%v#BS,Z0!;!@h5G]'
   }
   securityService(com.etrainu.shared.service.SecurityService) { bean ->
	   bean.autowire = "byName"
   }
   
   //Overrides the success handler after a user authenticates
   authenticationSuccessHandler(AuthenticationSuccessHandler) {
      def conf = SpringSecurityUtils.securityConfig

      requestCache = ref('requestCache')
      redirectStrategy = ref('redirectStrategy')
      defaultTargetUrl = conf.successHandler.defaultTargetUrl
      alwaysUseDefaultTargetUrl = conf.successHandler.alwaysUseDefault
      targetUrlParameter = conf.successHandler.targetUrlParameter
      ajaxSuccessUrl = conf.successHandler.ajaxSuccessUrl
      useReferer = conf.successHandler.useReferer
   }
}
