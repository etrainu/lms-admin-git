grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
//grails.project.war.file = "target/${appName}-${appVersion}.war"

def commonDirectory = new File("../lms-web-common")

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    repositories {
        grailsPlugins()
        grailsHome()
        grailsCentral()

        // uncomment the below to enable remote dependency resolution
        // from public Maven repositories
        //mavenLocal()
        mavenCentral()
        //mavenRepo "http://snapshots.repository.codehaus.org"
        mavenRepo "http://repository.codehaus.org"
        //mavenRepo "http://download.java.net/maven/2/"
        //mavenRepo "http://repository.jboss.com/maven2/"
    }
	plugins {
		if( !commonDirectory.exists() ) {
			println "Loading lms-web-common SNAPSHOT plugin from maven"
			compile ":lms-web-common:SNAPSHOT"
		}
	}
    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.

        // runtime 'mysql:mysql-connector-java:5.1.13'
		
		compile("org.codehaus.groovy.modules.http-builder:http-builder:0.5.1") {
			excludes 'groovy', 'xml-apis'
		}
    }
}

if( commonDirectory.exists() ) {
	println "Loading lms-web-common directory-based plugin"
	grails.plugin.location.'lms-web-common' = "../lms-web-common"
}


