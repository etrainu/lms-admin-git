class UrlFilters {
	//The purpose of this filter is to expose the controller and action name of the current request
	//to gsp pages for usage.
	def filters = {
		controllerDetails(controller:'*', action:'*') {
			before = {
				//for some reason controllername is writing out as controller/action
				//Parse it in this case
				flash.controllerName = controllerName.toString().toLowerCase()
				if( flash.controllerName.matches(/^[a-z]+\/[a-z]+$/) ) {
					flash.controllerName = controllerName.find(/^[a-z]+/)
					flash.actionName = controllerName.find(/[a-z]+$/)
				} else {
					flash.actionName = actionName
				}
			}
		}
	}
}
