import org.apache.log4j.ConsoleAppender
import org.apache.log4j.DailyRollingFileAppender
import org.apache.log4j.Priority

// locations to search for config files that get merged into the main config
// config files can either be Java properties files or ConfigSlurper scripts

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if(System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

grails {
	project.groupId = appName // change this to alter the default package name and Maven publishing destination
	mime {
		file.extensions = true // enables the parsing of file extensions from URLs into the request format
		use.accept.header = false
		types = [
			html: ['text/html','application/xhtml+xml'],
			xml: ['text/xml', 'application/xml'],
			text: 'text/plain',
			js: 'text/javascript',
			rss: 'application/rss+xml',
			atom: 'application/atom+xml',
			css: 'text/css',
			csv: 'text/csv',
			all: '*/*',
			json: ['application/json','text/json'],
			form: 'application/x-www-form-urlencoded',
			multipartForm: 'multipart/form-data'
		]
	}
	
	views.default.codec = "none" // none, html, base64
	views.gsp.encoding = "UTF-8"
	views.gsp.sitemesh.preprocess = true // enable Sitemesh preprocessing of GSP pages
	converters.encoding = "UTF-8"
	
	scaffolding.templates.domainSuffix = 'Instance' // scaffolding templates configuration
	
	//Grails server component
	legacy.server.host = 'localhost'
	legacy.server.port = '9001'
	legacy.server.app = 'lms-legacy-server'
}

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// whether to install the java.util.logging bridge for sl4j. Disable for AppEngine!
grails.logging.jul.usebridge = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

/**
* Directory configuration.
* Pickup the Tomcat/Catalina directory else use the target or current dir.
*/
def fs = File.separator // Local variable.
globalDirs.targetDir = new File("target${fs}").isDirectory() ? "target${fs}" : ''
globalDirs.catalinaBase = System.properties.getProperty('catalina.base')
globalDirs.logDirectory = globalDirs.catalinaBase ? "${globalDirs.catalinaBase}${fs}logs${fs}" : globalDirs.targetDir

//Default logging level for console and file appender
def log4jConsoleLogLevel = Priority.WARN
def log4jAppFileLogLevel = Priority.INFO
launchSQLDebug=false

// set per-environment serverURL stem for creating absolute links
environments {
    production {
		log4jConsoleLogLevel = Priority.DEBUG
		log4jAppFileLogLevel = Priority.DEBUG
		
		grails.legacy.server.port = '9002'
		grails.legacy.server.app = 'lms-legacy-server'
		
		grails.serverURL = "http://admin.etrainu.com"
    }
    development {
		log4jConsoleLogLevel = Priority.DEBUG
		log4jAppFileLogLevel = Priority.DEBUG
		
		//Tomcat 5.5 runs on port 8090 and version 0.1 of the server in the staging environment
		grails.legacy.server.port = '8090'
		grails.legacy.server.app = 'lms-legacy-server'
		
        //grails.serverURL = "http://dev.admin.etrainu.com:889"
    }
	staging
	{
		//Overrides for Staging
		log4jConsoleLogLevel = Priority.DEBUG
		log4jAppFileLogLevel = Priority.DEBUG
		
		//Tomcat 5.5 runs on port 8090 and version 0.1 of the server in the staging environment
		grails.legacy.server.port = '8090'
		grails.legacy.server.app = 'lms-legacy-server-0.1'
		
		grails.serverURL = "http://staging.admin.etrainu.com:888"
	}
    test {
		log4jConsoleLogLevel = Priority.DEBUG
		log4jAppFileLogLevel = Priority.DEBUG
        grails.serverURL = "http://localhost:8090/${appName}"
    }
	yoann {
		log4jConsoleLogLevel = Priority.DEBUG
		log4jAppFileLogLevel = Priority.DEBUG
		grails.gsp.enable.reload=true
		launchSQLDebug=false
	}
	steele {
		log4jConsoleLogLevel = Priority.DEBUG
		log4jAppFileLogLevel = Priority.DEBUG
		grails.gsp.enable.reload=true
		launchSQLDebug=false
	}
	tobias {
		log4jConsoleLogLevel = Priority.DEBUG
		log4jAppFileLogLevel = Priority.DEBUG
		grails.gsp.enable.reload=true
		launchSQLDebug=false
		grails.serverURL = "http://localhost:9001/${appName}"
	}

}

//Build Base URL after setting any overrides
grails.legacy.base.url = "http://${grails.legacy.server.host}:${grails.legacy.server.port}/${grails.legacy.server.app}"
userActivityApiString = "${grails.legacy.base.url}/userActivity/all/"
courseActivityApiString = "${grails.legacy.base.url}/courseActivity/all/"
salesActivityApiString = "${grails.legacy.base.url}/salesActivity/sales/"
userPartnershipApiString = "${grails.legacy.base.url}/user/partnership/"
userSearchApiString = "${grails.legacy.base.url}/user/search/"
userActionApiString = "${grails.legacy.base.url}/user/user/"
userRoleEditPermissionsApiString = "${grails.legacy.base.url}/user/editPermissions/"
userArchiveApiString = "${grails.legacy.base.url}/user/archive/"
userUnarchiveApiString = "${grails.legacy.base.url}/user/unarchive/"
groupListApiString = "${grails.legacy.base.url}/group/list/"

courseSearchApiString = "${grails.legacy.base.url}/course/search"
courseArchiveApiString = "${grails.legacy.base.url}/course/archive/"
courseUnarchiveApiString = "${grails.legacy.base.url}/course/unarchive/"
courseGetApiString = "${grails.legacy.base.url}/course/get"

referrerSearchApiString = "${grails.legacy.base.url}/referrer/search/"
referrerActivityApiString = "${grails.legacy.base.url}/referrerActivity/sale/"
referrerArchiveApiString = "${grails.legacy.base.url}/referrer/archive/"
referrerUnarchiveApiString = "${grails.legacy.base.url}/referrer/unarchive/"
referrerActionApiString = "${grails.legacy.base.url}/referrer/referrer/"

securityUserForAuthenticationApiString = "${grails.legacy.base.url}/security/userForAuthentication/"
securityAuthenticationApiString = "${grails.legacy.base.url}/security/authenticate/"

//////////////////////////////////////////////////////////////////
// log4j configuration
//////////////////////////////////////////////////////////////////

log4j = {
  println "Log4j consoleLevel: ${log4jConsoleLogLevel} appFile Level: ${log4jAppFileLogLevel}"
 
  //def logLayoutPattern = new PatternLayout("%d [%t] %-5p %c %x - %m%n")
 
  error 'org.codehaus.groovy.grails.commons', // core / classloading
		  'org.codehaus.groovy.grails.plugins', // plugins
		  'org.codehaus.groovy.grails.orm.hibernate', // hibernate itg
		  'org.springframework',
		  'org.hibernate',
		  'net.sf.ehcache.hibernate',
		  'grails',
		  'groovyx.net.http'
 
  info 'org.codehaus.groovy.grails.web.servlet',  //  controllers
		  'org.codehaus.groovy.grails.web.pages', //  GSP
		  'org.codehaus.groovy.grails.web.sitemesh', //  layouts
		  'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
		  'org.codehaus.groovy.grails.web.mapping' // URL mapping	
 
  debug 'grails.app.controller', //actual controllers
  		'com.etrainu',
  	    'org.apache.http.headers',
        'org.apache.http.wire'
		
		/* This is the list of grails.app artifacts for logging
		 * grails.app.bootstrap
		 * grails.app.dataSource
		 * grails.app.tagLib
		 * grails.app.service
		 * grails.app.controller
		 * grails.app.domain
		 */
		
 
  appenders {
	appender new ConsoleAppender(name: "${appName}-console",
		    threshold: log4jConsoleLogLevel,
			layout:pattern(conversionPattern: '[%t] %-5p %c{2} %x - %m%n')
	)
	appender new DailyRollingFileAppender(name: "${appName}",
			file:"${globalDirs.logDirectory}${appName}.log".toString(),
			threshold: log4jAppFileLogLevel,
			datePattern: "'.'yyyy-MM-dd",
			layout:pattern(conversionPattern: '%d{[EEE, dd-MMM-yyyy @ HH:mm:ss.SSS]} [%t] %-5p %c %x - %m%n')
	)
  }
 
  root {
	error "${appName}-console", "${appName}"
	additivity = true
  }
}

avatarPlugin {
	defaultGravatarUrl="http://www.etrainu.com/assets/images/user_blank.jpg"
	gravatarRating="G"
}

// TODO: Comment when we figure out how to deploy the reloadable appConfig to a war file
api.maxResultsDisplayed = 400
api.maxResultsDisplayedForDepartement = 600

//Reloadable config info
grails.config.locations = ["file:./AppConfig.groovy"]
grails.plugins.reloadConfig.files = []
grails.plugins.reloadConfig.includeConfigLocations = true
grails.plugins.reloadConfig.interval = 5000
grails.plugins.reloadConfig.enabled = true
grails.plugins.reloadConfig.notifyPlugins = [];
