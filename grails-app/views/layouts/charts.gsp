<!DOCTYPE html>
<html>
    <head>
        <title><g:layoutTitle default="Grails" /></title>
        <link rel="stylesheet" href="${resource(dir:'css',file:'main.css')}" />
        <link rel="shortcut icon" href="${resource(dir:'images',file:'favicon.ico')}" type="image/x-icon" />
		<g:javascript src = 'jquery-1.5.2.min.js'/>
		<g:javascript src = 'jquery-ui-1.8.11.min.js'/>
	    <g:javascript src = 'highcharts.js' />
        <g:layoutHead />
    </head>
    <body>
        <g:layoutBody />
    </body>
</html>