<!DOCTYPE html>
<html>
    <head>
    	  <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
  		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  		
        <title><g:layoutTitle default="Grails" /></title>
        <link rel="shortcut icon" href="${resource(dir:'images',file:'icon.ico')}" type="image/x-icon"/>
		<link rel="apple-touch-icon" href="${resource(dir:'images',file:'iphone3gIcon.png')}"/>
		<link rel="apple-touch-icon" sizes="72x72" href="${resource(dir:'images',file:'iphone3gIcon.png')}"/>
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir:'images',file:'iphone4logo.png')}"/>
 		<!-- <link rel="stylesheet" href="${resource(dir:'css',file:'jquery.mobile-1.0b3.css')}" /> -->
 		
 		
        <link rel="stylesheet" href="http://code.jquery.com/mobile/1.0b3/jquery.mobile-1.0b3.min.css" />
 		<link rel="stylesheet" href="${resource(dir:'css',file:'jquery.ui.datepicker.mobile.css')}" />
        <link rel="stylesheet" href="${resource(dir:'css',file:'mobile.css')}" />
        
		<g:javascript src = 'jquery-1.6.2.min.js' />
		<g:javascript src = 'jquery-ui-1.8.11.min.js'/>
		<g:javascript src = 'jquery-ui-datepicker-1.8.5.min.CUSTOM.js' />
	    <g:javascript src = 'highcharts.js' />
		<script type="text/javascript">
			$(document).bind("mobileinit", function(){
				  $.mobile.defaultPageTransition = 'none';
				  $.mobile.page.prototype.options.degradeInputs.date = true;
			});

			
		</script>
		<g:javascript src = "jquery.mobile-1.0b3.min.js" />
		<g:javascript src = "jquery.mobile.datePicker.js" />
 		<g:javascript src = 'contextListview.js' />
 		<g:javascript src = 'common.js' />

        <link rel="shortcut icon" href="${resource(dir:'images',file:'favicon.ico')}" type="image/x-icon" />
        <g:javascript library="application" />
        <g:layoutHead />
    </head>
    <body>
        <g:layoutBody />
    </body>
</html>