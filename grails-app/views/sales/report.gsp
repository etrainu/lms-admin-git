<%@ page import="com.etrainu.user.UserActivity.UserActivityType"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.etrainu.user.UserActivity" %>
<%@ page import="com.etrainu.user.User" %>

<html>
<head>
<title>etrainu mobile admin</title>
<meta name="layout" content="main" />
</head>
<body>
	<div data-role="page">
		<script type="text/javascript">
		      var chart;
				$(document).ready(function() {
			   	chart = new Highcharts.Chart({
			      chart: {
			         renderTo: 'activityChart',
			         type: 'spline'
			      },
			      title: {
			         text: 'Sales Activity '
			      },
			      subtitle: {
			         text: '----------------------'   
			      },
			      xAxis: {
			         type: 'datetime'
			      },
			      yAxis: {
			         title: {
			            text: 'Numbers Sold'
			         },
			         min: 0
			      },
			      tooltip: {
			         formatter: function() {
			                   return '<b>'+ this.series.name +'</b><br/>'+
			                   Highcharts.dateFormat('%A %B %e %Y', this.x) +': '+ this.y;
			         }
			      },  
			      series: [			      
			      <g:each in="${activityMap}" var="entry">
			      			{
			      				<g:set var="totalSold" value="${0}" />
			      				<g:set var="totalIncome" value="${0}" />
								data : [
										<g:each in="${entry.value}" var="a">
											[Date.UTC(${a.salesActivityDate.year + 1900},${a.salesActivityDate.month},${a.salesActivityDate.date}),${(a.salesActivityCount?a.salesActivityCount:0)}],
											<g:set var="totalSold" value="${totalSold + a.salesActivityCount}"/>
											<g:set var="totalIncome" value="${totalIncome + a.salesActivityIncome}"/>			
										</g:each>
										],
			      				name: "${entry.key.name} [${totalSold}][$${totalIncome}]"
			      			},
			      </g:each>
			   	  ]
			   });
		   });      
		</script>
	
		<!--<g:include view="/include/header.gsp" />-->		
		<g:include view="include/navigation.gsp" />
	
		<!-- end header -->
		<div data-role="content">
			<div class="ui-body">
				<g:datePickerForm controller="sales" action="report" id="${params.id}" fromDateValue="${fromDate}" toDateValue="${toDate}" />
				<br/>
				<div id="activityChart" style="width:100%;height:400px"></div>	
			</div>		
		</div>
		<!--  end content -->
		<g:include view="/include/footer.gsp" />
		</div>
		<!-- end page -->
</body>
</html>