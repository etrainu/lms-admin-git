<%@ page import="com.etrainu.referrer.ReferrerActivity.ReferrerActivityType" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.etrainu.referrer.ReferrerActivity" %>
<%@ page import="com.etrainu.referrer.Referrer" %>

<html>
<head>
<title>etrainu mobile admin</title>
<meta name="layout" content="main" />
</head>
<body>
	<div data-role="page" class="type-interior">
		<script type="text/javascript">
		var chart;
		$(document).ready(function () {
			chart = new Highcharts.Chart({
				chart: {
					renderTo: 'activityChart',
				},
				title: {
					text: 'Sales Activity '
				},
				subtitle: {
					text: '----------------------'
				},
				xAxis: {
					type: 'datetime'
				},
				yAxis: [{
					title: {
						text: 'Sales'
					},
					min: 0
				}, {
					labels: {
						formatter: function () {
							return '$' + this.value;
						},
						style: {
							color: '#AA4643'
						}
					},
					title: {
						text: 'Income',
						style: {
							color: '#AA4643'
						}
					},
					min: 0,
					opposite: true
				}],
				tooltip: {
					formatter: function () {
						var unit = '';
						if( this.series.name.match(/^Income/) ) {
							unit = "$"
						}

						return '<b>' + this.series.name + '</b><br/>' + Highcharts.dateFormat('%A %B %e %Y', this.x) + ': ' + unit + this.y;
					}
				},
				series: [{
					color: '#4572A7',
					type: 'column',
					yAxis: 0,
					data: [ 
						<% total = 0 %> 
						<g:each in="${activities}" var="a"> 
							<g:if test="${a.referrerActivityDate && a.referrerActivityType == ReferrerActivityType.SALES}"> 
								[Date.UTC(${a.referrerActivityDate.year + 1900}, ${a.referrerActivityDate.month}, ${a.referrerActivityDate.date}), ${(a.referrerActivityCount ? a.referrerActivityCount : 0)}],
							</g:if>
							<% total += (a.referrerActivityCount?a.referrerActivityCount:0) %>
						</g:each> 
					],
					name: "Sales [${total} unit]"
				}, {
					color: '#AA4643',
					type: 'spline',
					yAxis: 1,
					data: [ 
						<% total = 0 %> 
						<g:each in="${activities}" var="b"> 
							<g:if test="${b.referrerActivityDate && b.referrerActivityType == ReferrerActivityType.SALES}"> 
								[Date.UTC(${ b.referrerActivityDate.year + 1900 }, ${b.referrerActivityDate.month}, ${b.referrerActivityDate.date}), ${(b.referrerActivityIncome ? b.referrerActivityIncome : 0)}],
							</g:if>
							<% total += (b.referrerActivityIncome?b.referrerActivityIncome:0) %>
						</g:each> 
					],
					name: "Income [${total}]"
				}]
			});
		});
		</script>

		<g:include view="/include/header.gsp" />	
	
		<!-- end header -->
		<div data-role="content">
			<div class="ui-body">
				<g:datePickerForm controller="referrer" action="report" id="${params.id}" fromDateValue="${fromDate}" toDateValue="${toDate}" />
				<br />
				<div id="activityChart" style="width:100%;height:400px"></div>
				<p>Note* The date range is non-inclusive of events on ${toDate}. Actual data range is 00:00:00 ${fromDate} to 00:00:00 ${toDate}.</p>	
			</div>		
		</div>
		<!--  end content -->
		<g:include view="/include/footer.gsp" />
		</div>
		<!-- end page -->
		
</body>
</html>