<html>
<head>
<title>etrainu mobile admin</title>

<meta name="layout" content="main" />
</head>
<body>
	<div data-role="page" class="type-interior">

		<g:include view="include/navigation.gsp" />
		
		<script>
			archiveReferrerUrl = '${createLink(controller:"referrer",action:"archiveReferrer_ajax")}';
			unarchiveReferrerUrl = '${createLink(controller:"referrer",action:"unarchiveReferrer_ajax")}';
		</script>
		
		<g:javascript src='referrer.js' />
		
		<!-- end header -->
		<div data-role="content">
			<div class="ui-body">
				<g:form controller="referrer" action="search" data-type="vertical" data-ajax="false">
					<div class="referrer-search">
						<input type="search" class="ui-body ui-body-c" name="name"
							id="name" value="${ params.name }"
							placeholder="Referrer Name..." />
							
						<input type="checkbox" name="archived" id="archived_on" value="true" ${(params.archived.equals('true'))?'checked="checked"':''} />
						<label for="archived_on">Arch</label>
	
						<input type="submit" id="searchButton" value="Search"
							data-icon="search" data-inline="true" />
							
						<sec:ifAnyGranted roles="ROLE_EDIT_REFERRER">
							<g:link controller="referrer" action="editReferrer" rel="external" data-role="button" data-inline="true" data-theme="c" data-icon="plus">Create</g:link>
						</sec:ifAnyGranted> 
					</div>
				</g:form>
			</div>

			<% now = new Date() %>
			<ul data-role="context-listview" data-inset="true">
				<g:each var="r" in="${referrers}">
				<%
					lastPurchaseText = 'Never';
					lastPurchaseClass = 'error';
					if( r.lastPurchase) {
						lastPurchaseText = r.lastPurchase.format( "dd / MM / yyyy" )
						dayDiff = Math.ceil( ( now.getTime() - r.lastPurchase.getTime() ) / (8.64 * 10000000) )
						if( dayDiff < 365 ) {
							lastPurchaseClass = '';
						}
					}
					
					theme = ( r.archiveDate ) ? 'r' : '' ;
					
				 %>
				
					<li data-theme="${theme}" data-referrerid="${r.id}" data-archived="${r.archiveDate!=null}">
					<g:link controller="referrer" action="report" params="[id:r.id]" rel="external" class="allowWrap">
							<div>
								${r.domain} - ${r.name} 
							</div>
							<div class="completion-orange">
							   <g:if test="${r.isDiscount}">
								<g:if test="${!r.isPercentage}">$</g:if><g:if test="${r.discount}">${r.discount}</g:if><g:else>-</g:else><g:if test="${r.isPercentage}">%</g:if>
							   </g:if>
							   <g:else>
							   	 Ref
							   </g:else>
							</div>
							
							<div class="referrer-details">	
								<div id="referrer-detail">
									Purchases: <g:if test="${r.totalPurchase}">${r.totalPurchase}</g:if><g:else>none</g:else>
								</div>
								<div id="referrer-detail">
									Income: <g:if test="${r.totalAmount}">$${r.totalAmount}</g:if><g:else>none</g:else>
								</div>
								
							</div>
							<div class="referrer-details">
								<div id="referrer-detail">
									Last Used: <span class="${lastPurchaseClass}">${ lastPurchaseText }</span>
								</div>
								<g:if test='${r.archiveDate != null}'>
									<div id="referrer-detail">
										Archive Date: <g:formatDate format="dd / MM / yyyy" date="${r.archiveDate }" />
									</div>
									</g:if>
								</div>
							</g:link>
							
							<div data-role="menu" data-icon="gear">
							
						<g:link controller="referrer" action="report" params="[id:r.id]" rel="external">Activity Report</g:link>
						
							<sec:ifAnyGranted roles="ROLE_EDIT_REFERRER">
								<g:link controller="referrer" action="editReferrer" params="[id:r.id]"
									rel="external">Edit Referrer</g:link>
							</sec:ifAnyGranted>
							<sec:ifAnyGranted roles="ROLE_ARCHIVE_REFERRER">
								<a href="#" class="archiveReferrer">Archive Referrer</a>
								<a href="#" class="unarchiveReferrer">Reinstate Referrer</a>
							</sec:ifAnyGranted>
						</div>
					</li>
					
				</g:each>
			</ul>
		</div>
		<!--  end content -->
		<g:include view="/include/footer.gsp" />
	</div>
	<!-- end page -->
</body>
</html>