<html>
<head>
<title>etrainu mobile admin</title>
<meta name="layout" content="main" />
</head>
<body>
	<div data-role="page" class="type-interior">

		<g:include view="/include/header.gsp" />
		<g:javascript src='editForm.js' />
		<script type="text/javascript">
			$(function() {
				$('input[name="isDiscount"]').change(function() {
					var self = this,
						jSelf = $(this)
					;
					
					if( self.value == 0 ) {
						//disable the others
						disableFields();
					} else {
						enableFields();
					}
				});

				function enableFields() {
					$('input[name="isPercentage"]')
						.removeAttr('disabled')
						.parent()
						.removeClass('ui-disabled')
					;
					$('#discount')
						.removeAttr('disabled')
						.removeClass('ui-input-text-disabled')
					;
				}

				function disableFields() {
					$('input[name="isPercentage"]')
						.attr('disabled', 'disabled')
						.parent()
						.addClass('ui-disabled')
					;
					
					$('#discount')
						.attr('disabled', 'disabled')
						.addClass('ui-input-text-disabled')
					;
				}

				//ensure the field starts enabled
				enableFields();
				<g:if test="${ referrer.isDiscount == null || !referrer.isDiscount.toBoolean() }">
					disableFields();
				</g:if>
			});
		</script>
		<!-- end header -->
		<div data-role="content">
			<g:if test="\${flash.message}">
				<div class="message">
					<p>${flash.message}</p>
				</div>
			</g:if>

			<!-- disable editing domain / code -->
			<%
				disabledString = "disabled"
				disabledClass = "ui-input-text-disabled"
				if(!referrer.id) {
					disabledClass = ""
					disabledString = ""
				}
				
				isDiscountFlag = ""
				isNotDiscountFlag = ""
				
				if( referrer.isDiscount && referrer.isDiscount.toBoolean() ) {
					isDiscountFlag = "checked"
				} else {
					isNotDiscountFlag = "checked"
				}
				
				isPercentageFlag = ""
				isNotPercentageFlag = ""
				
				if(referrer.isPercentage && referrer.isPercentage.toBoolean()) {
					isPercentageFlag = "checked"
				}
				else {
					isNotPercentageFlag = "checked"
				}
			 %>
			 
			<%-- 
				If you are looking for what causes this to be an ajax call seperate from JQMobile
				Look in editForm.js
			 --%>
			<g:form id="editForm" name="editForm" action="updateReferrer_ajax" id="${referrer.id}" data-ajax="false">

				<div id="referrernameDiv" class="ui-form-field">
					<label for="name">Referrer Name:</label> <input id="name"
						name="name" type="text" value="${ referrer.name }" />
				</div>
				
				<div id="referrerdomainDiv" class="ui-form-field">
					<label for="domain">Referrer Domain/Code:</label> <input id="domain"
						name="domain" type="text" value="${ referrer.domain }" class="${disabledClass}" ${disabledString} />
				</div>
				
				<div class="ui-form-field">
				    <fieldset data-role="controlgroup">
				    	<legend>Type:</legend>
				         	<input type="radio" name="isDiscount" id="radio-choice-2" value="0" ${isNotDiscountFlag} />
				         	<label for="radio-choice-2">Referrer</label>
				         	
				         	<input type="radio" name="isDiscount" id="radio-choice-1" value="1" ${isDiscountFlag} />
				         	<label for="radio-choice-1">Discount Code</label>
				    </fieldset>
				</div>
				
				<div class="ui-form-field">
				    <fieldset data-role="controlgroup" id="isPercentageGroup">
				    	<legend>Discount Type:</legend>
				         	<input type="radio" name="isPercentage" id="radio-choice-1" value="1" ${isPercentageFlag}/>
				         	<label for="radio-choice-1">Percentage</label>
				
				         	<input type="radio" name="isPercentage" id="radio-choice-2" value="0" ${isNotPercentageFlag}/>
				         	<label for="radio-choice-2">Dollars</label>
				    </fieldset>
				</div>							
				
				<div id="referrerdiscountDiv" class="ui-form-field">
					<label for="discount">Discount Value:</label> <input id="discount"
						name="discount" type="text" value="${ referrer.discount }" />
				</div>

				<button type="submit" name="doEdit" data-theme="b">Save</button>

			</g:form>
		</div>
		<!--  end content -->
		<g:include view="/include/footer.gsp" />
	</div>
	<!-- end page -->
</body>
</html>