<div data-role="footer" class="footer-docs" data-theme="c">
	<p>&copy; 2011 etrainu - LMS Administration v${grailsApplication.metadata['app.version']}
		<sec:ifLoggedIn>
	 		- Logged in as <sec:username/>
		</sec:ifLoggedIn>	
	 </p>
	
				
	<sec:ifLoggedIn>
		<g:link controller="logout" rel="external" id="logout" data-icon="home" class="logoutButton">Logout</g:link>
	</sec:ifLoggedIn>	
</div>