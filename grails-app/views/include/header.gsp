<div data-role="header" data-theme="y" data-add-back-btn="true">
	<g:if test="${title != null}">
		<h1>${title}</h1>	
	</g:if>
	<g:else>
		<h1>Administration</h1>
	</g:else>
	<a href="" data-icon="arrow-l" data-theme="b" data-rel="back">Back</a>
</div>