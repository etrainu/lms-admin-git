<%
	def navGroup = 1
	if( flash.controllerName == 'course' ) {
		navGroup = 2
	}
	if ( flash.controllerName == 'referrer' ) {
		navGroup = 3
	}
	if ( flash.controllerName == 'sales' ) {
		navGroup = 4
	}
%>

<script>
	//workaround to stop the firstfocus messing up the navigation
	$( document ).bind( "pagecreate create", function( e ) {
		$(e.target).data('lastClicked', $("<a></a>") );
	});
</script>
<div data-role="header" data-theme="b"><!-- start header -->
	<div data-role="navbar"><!-- start navbar -->
		<ul>			
			<%-- Must have ROLE_ADMIN to view this --%>
			<sec:ifAnyGranted roles="ROLE_VIEW_USER">
				<li>
					<g:link	controller="user" action="user" class="${(navGroup==1)?'ui-btn-active':''}"
					data-icon="star" rel="external">Users</g:link>
				</li>
			</sec:ifAnyGranted>
	
			<%-- Must have ROLE_ADMIN to view this --%>
			<sec:ifAnyGranted roles="ROLE_VIEW_COURSE">
				<li>
					<g:link	controller="course" action="search" class="${(navGroup==2)?'ui-btn-active':''}"
					data-icon="grid"  rel="external">Courses</g:link>
				</li>
			</sec:ifAnyGranted>
			
			<%-- Must have ROLE_ADMIN to view this --%>
			<sec:ifAnyGranted roles="ROLE_VIEW_REFERRER">
				<li><g:link	controller="referrer" action="search" class="${(navGroup==3)?'ui-btn-active':''}"
				data-icon="gear" rel="external">Referrer</g:link></li>
			</sec:ifAnyGranted>
			
			<sec:ifAnyGranted roles="ROLE_VIEW_SALES_FIGURES_REPORT">
				<li><g:link	controller="sales" action="report" class="${(navGroup==4)?'ui-btn-active':''}"
				data-icon="gear" rel="external">Sales Report</g:link></li>
			</sec:ifAnyGranted>
					
		</ul>
	</div><!-- end navbar -->
</div><!-- end header -->