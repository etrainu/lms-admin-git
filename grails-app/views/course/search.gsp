<html>
<head>
<title>etrainu mobile admin</title>

<meta name="layout" content="main" />
</head>
<body>
	<div data-role="page" class="type-interior">

		<!--<g:include view="/include/header.gsp" />-->
		<g:include view="include/navigation.gsp" />
		
		<script>
			archiveCourseUrl = '${createLink(controller:"course",action:"archiveCourse_ajax")}';
			unarchiveCourseUrl = '${createLink(controller:"course",action:"unarchiveCourse_ajax")}';
		</script>
		<g:javascript src='course.js' />

		<!-- end header -->
		<div data-role="content">
			<div class="ui-body">
				<g:form controller="course" action="search" data-type="vertical" data-ajax="false">			
				<div class="course-search">		
					<input type="search" class="ui-body ui-body-c" name="searchString"
						id="name" value="${ params.searchString }"
						placeholder="Course Name..." />
						
					<input type="checkbox" name="archived" id="archived_on" value="true" ${(params.archived.equals('true'))?'checked="checked"':''} />
					<label for="archived_on">Arch</label>

					<input type="submit" id="searchButton" value="Search"
						data-icon="search" data-inline="true" />					
				</div>
				</g:form>
			</div>
			<!--  end collapsible -->

			<g:if test="${ courses.size() < totalCourses }">
				<h5>
					Your search returned
					${totalCourses}
					results or more but only
					${courses.size()}
					results were displayed.<br /> Consider refining your search
					criteria.
				</h5>
			</g:if>

			<% now = new Date() %>
			<ul data-role="context-listview" data-inset="true">
				<g:each var="c" in="${courses}">
					<%
						percentCompleteClass = 'completion-red';
						percentComplete = 0;
						if( c.participantCount && c.completedCount ) {
							percentComplete = Math.round( 100 * (c.completedCount/ c.participantCount) );
							if ( percentComplete > 50 && percentComplete < 80 )
							{
								percentCompleteClass = 'completion-orange';
							}
							if( percentComplete >= 80 ) {
								percentCompleteClass = 'completion-green';
							}
						}
						
						lastEnrolmentText = 'Never';
						lastEnrolmentClass = 'error';
						if( c.lastEnrolment) {
							lastEnrolmentText = c.lastEnrolment.format( "dd / MM / yyyy" )
							dayDiff = Math.ceil( ( now.getTime() - c.lastEnrolment.getTime() ) / (8.64 * 10000000) )
							if( dayDiff < 365 ) {
								lastEnrolmentClass = '';
							}
						}
						
						theme = ( c.archiveDate ) ? 'r' : '' ;
					%>
					<li data-theme="${theme}" data-courseid="${c.id}" data-archived="${c.archiveDate!=null}">
						<g:link controller="course" action="report" params="[id:c.id]" rel="external" class="allowWrap">
							<div>
								${c.inductionName} <g:defaultText value="(${c.courseCode})" default="" test="${!c.courseCode}" />
							</div>
						
							<div class="${percentCompleteClass}">
								${percentComplete}%
							</div>
							<div class="course-details">
								
								<div id="course-detail">
									Standalone: ${c.isStandalone}
								</div>
								<div id="course-detail">
									#Certificates: ${c.certificateCount}
								</div>
								<div id="course-detail">
									#Categories: ${c.categoryCount}
								</div>
							</div>
							<div class="course-details">
								<div id="course-detail">
									Scorm: ${c.isScorm}
								</div>
								
								<div id="course-detail">
									Feedback: ${c.feedbackRequired}
								</div>
								<div id="course-detail">
									#Bundles/#TPlans:	${c.bundleCount}/${c.trainingPlanCount}
								</div>
							</div>
							<div class="course-details">
								<div id="course-detail">
									#Enrolments/#Comp: ${c.participantCount}/${c.completedCount}
								</div>
								<div id="course-detail">
									#Expired/#Deactivate: ${c.expiredCount}/${c.deactivatedCount}
								</div>
								<div id="course-detail">
									Avg Completion: <g:totalTime time="${c.averageCompletionMinutes}" />
								</div>
							</div>
							<div class="course-details">								
								<div id="course-detail">
									First Enrol: <g:defaultText value="${c.firstEnrolment}" format="dd / MM / yyyy" defaultClass="error" default="Never" />
								</div>
								<div id="course-detail">
									Last Enrol:	<span class="${lastEnrolmentClass}">${ lastEnrolmentText }</span>
								</div>
								<div id="course-detail">
									Supplier:	${c.supplier}
								</div>
									
								<g:if test='${c.archiveDate != null}'>
								<div id="course-detail">
									Archive Date: <g:formatDate format="dd / MM / yyyy" date="${ c.archiveDate }" />
								</div>
								</g:if>
							</div>
							<p class="ui-li-count ui-btn-up-c ui-btn-corner-all">
								Price: $${c.cost} <g:defaultText value="(cust)" default="" test="${!c.customPricing}" />
							</p>
						</g:link>

						<div data-role="menu" data-icon="gear">
							<g:link controller="course" action="report" params="[id:c.id]" rel="external" class="allowWrap">Activity Report</g:link>
							<sec:ifAnyGranted roles="ROLE_VIEW_COURSE">
								<g:link controller="course" action="categoryList"
									params="[id:c.id]" rel="external">Category List</g:link>
							</sec:ifAnyGranted>
							<%--<g:link controller="course" action="customPricing"
								params="[id:c.id]" rel="external">Custom Pricing</g:link>
							<g:link controller="course" action="bundleMembership"
								params="[id:c.id]" rel="external">Bundle Membership</g:link>
							<g:link controller="course" action="accessList"
								params="[id:c.id]" rel="external">Access List</g:link>
							 --%>
							<sec:ifAnyGranted roles="ROLE_ARCHIVE_COURSE">
								<a href="#" class="archiveCourse">Archive Course</a>
								<a href="#" class="unarchiveCourse">Reinstate Course</a>
							</sec:ifAnyGranted>
						</div></li>
				</g:each>
			</ul>
		</div>
		<!--  end content -->
		<g:include view="/include/footer.gsp" />
	</div>
	<!-- end page -->
</body>
</html>