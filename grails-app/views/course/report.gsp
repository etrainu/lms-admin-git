<%@page import="com.etrainu.user.CourseActivity.CourseActivityType"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.etrainu.user.CourseActivity" %>
<%@ page import="com.etrainu.course.Course" %>

<html>
<head>
<title>etrainu mobile admin</title>
<meta name="layout" content="main" />
</head>
<body>
	<div data-role="page" class="type-interior">
		<script type="text/javascript">
			var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'activityChart',
						type: 'spline'
					},
					title: {
						text: 'Course Activity'
					},
					subtitle: {
						text: '----------------------'   
					},
					xAxis: {
						type: 'datetime'
					},
					yAxis: {
					title: {
						text: 'Activity'
					},
					min: 0
				},
				tooltip: {
					formatter: function() {
						return '<b>'+ this.series.name +'</b><br/>'+
						Highcharts.dateFormat('%A %B %e %Y', this.x) +': '+ this.y;
					}
				},
				series: [{
					type : 'area',
					data: [
						<% total = 0 %>
						<g:each in="${activities}" var="a">
							<g:if test="${a.courseActivityDate && a.courseActivityType == CourseActivityType.COURSE_ASSIGNMENT}">
								[Date.UTC(${a.courseActivityDate.year + 1900},${a.courseActivityDate.month},${a.courseActivityDate.date}),${(a.courseActivityCount?a.courseActivityCount:0)}],
								<% total += (a.courseActivityCount?a.courseActivityCount:0) %>
							</g:if>
						</g:each>	
						],
						name: 'Course Assignment [${total}]'
					},
					{
						type : 'area',
						data: [
							<% total = 0 %>
							<g:each in="${activities}" var="a">
								<g:if test="${a.courseActivityDate && a.courseActivityType == CourseActivityType.COURSE_COMPLETION}">
									[Date.UTC(${a.courseActivityDate.year + 1900},${a.courseActivityDate.month},${a.courseActivityDate.date}),${(a.courseActivityCount?a.courseActivityCount:0)}],
									<% total += (a.courseActivityCount?a.courseActivityCount:0) %>
								</g:if>
							</g:each>	
							],
							name: 'Course Completion [${total}]'
						}]
				});
			});      
		</script>

		<g:include view="/include/header.gsp" />

		<!-- end header -->
		<div data-role="content">
			<div class="ui-body">
				<g:datePickerForm controller="course" action="report"
					id="${params.id}" fromDateValue="${fromDate}"
					toDateValue="${toDate}" />
				<br />
				<div id="activityChart" style="width: 100%; height: 400px"></div>
				<p>
					Note* The date range is non-inclusive of events on
					${toDate}. Actual data range is 00:00:00
					${fromDate}
					to 00:00:00
					${toDate}.
				</p>
			</div>
		</div>
		<!--  end content -->
		<g:include view="/include/footer.gsp" />
	</div>
	<!-- end page -->

</body>
</html>