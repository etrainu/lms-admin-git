<html>
<head>
<title>etrainu mobile admin</title>
<meta name="layout" content="main" />
</head>
<body>
	<div data-role="page" class="type-index">

		<div data-role="header" data-position="inline" data-theme="y">
			<h1>Login</h1>
		</div>
		<div data-role="content">
			<div class="ui-body ui-body-login">
				<p />
				<g:if test='${flash.message}'>
					<div class='login_message'>
						${flash.message}
					</div>
					<p />
				</g:if>

				<form action='${postUrl}' method='POST' id='loginForm' class='cssform' autocomplete='off' data-ajax="false">
					<label for='username'>Username</label> 
					<input type='text' class='text_' name='j_username' id='username' data-theme="c" />
					
					<br /><br />
					
					<label for='password'>Password</label> 
					<input type='password' class='text_' name='j_password' id='password' data-theme="c" />

					<label for='remember_me'>Remember me</label> 
					<input type='checkbox' class='chk' name='${rememberMeParameter}' 
					id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if> data-theme="c" />

					<input type='submit' value='Login' data-inline="true" />

				</form>
			</div>
			<!-- end of content -->

		</div>
		<!--  end page -->
		<g:include view="/include/footer.gsp" />
	</div>
</body>
</html>
