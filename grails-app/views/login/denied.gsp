<html>
    <head>
        <title>etrainu mobile admin</title>
        <meta name="layout" content="main" />          
    </head>
    <body>
	    <div data-role="page" class="type-index">
	
		<g:include view="/include/header.gsp" />

			<div data-role="content">
				<div class='errors'>Sorry, you're not authorized to view this page.</div>				
			</div><!-- end of content -->
			
			<g:include view="/include/footer.gsp" />
			
	    </div><!--  end page -->    
    </body>
</html>
