<html>
	<head>
	    <meta name="layout" content="main" />
	    <title>User Partnerships</title>
 
	</head>
	
	<body>
    	<h1>User Partnerships</h1>
 	
 		<table cellpadding='0' cellspacing='0'>
 			<tr>
 				<th>Suborg ID</th>
 				<th>Suborg</th>
 				<th>Department ID</th>
 				<th>Department</th>
 				<th>Organisation ID</th>
 				<th>Organisation</th>
 			</tr>
 			
	 		<g:each in="${partnerships}" var="partnership">
	 			<tr>
	 				<td>${partnership.suborgID}</td>
	 				<td>${partnership.suborgName}</td>
	 				<td>${partnership.deptID}</td>
	 				<td>${partnership.deptName}</td>
	 				<td>${partnership.orgID}</td>
	 				<td>${partnership.orgName}</td>
	 			</tr>
			</g:each>
		</table>
	</body>
</html>