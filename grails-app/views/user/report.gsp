<%@ page import="com.etrainu.user.UserActivity.UserActivityType"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.etrainu.user.UserActivity" %>
<%@ page import="com.etrainu.user.User" %>

<html>
<head>
<title>etrainu mobile admin</title>
<meta name="layout" content="main" />
</head>
<body>
	<div data-role="page">
		<script type="text/javascript">
		      var chart;
				$(document).ready(function() {
			   	chart = new Highcharts.Chart({
			      chart: {
			         renderTo: 'activityChart',
			         type: 'spline'
			      },
			      title: {
			         text: 'User Activity '
			      },
			      subtitle: {
			         text: '----------------------'   
			      },
			      xAxis: {
			         type: 'datetime'
			      },
			      yAxis: {
			         title: {
			            text: 'Activity'
			         },
			         min: 0
			      },
			      tooltip: {
			         formatter: function() {
			                   return '<b>'+ this.series.name +'</b><br/>'+
			                   Highcharts.dateFormat('%A %B %e %Y', this.x) +': '+ this.y;
			         }
			      },
			      series: [{
					         type: 'column',
					         data: [
							    <% total = 0 %>
								<g:each in="${activities}" var="a">
									<g:if test="${a.userActivityDate && a.userActivityType == UserActivityType.LOGIN}">
										[Date.UTC(${a.userActivityDate.year + 1900},${a.userActivityDate.month},${a.userActivityDate.date}),${(a.userActivityCount?a.userActivityCount:0)}],
										<% total += (a.userActivityCount?a.userActivityCount:0) %>
									</g:if>
								</g:each>	
					         ],
							name: 'Login [${total}]'
				         },
				         {
					         data: [
								<% total = 0 %>
								<g:each in="${activities}" var="a">
									<g:if test="${a.userActivityDate && a.userActivityType == UserActivityType.PURCHASE}">
										[Date.UTC(${a.userActivityDate.year + 1900},${a.userActivityDate.month},${a.userActivityDate.date}),${(a.userActivityCount?a.userActivityCount:0)}],
									</g:if>
									<% total += (a.userActivityCount?a.userActivityCount:0) %>
								</g:each>	
					         ],
				         	 name: 'Purchases [${total}]'
					      },
				         {
					         data: [
								<% total = 0 %>
								<g:each in="${activities}" var="a">
									<g:if test="${a.userActivityDate && a.userActivityType == UserActivityType.COURSE_ASSIGNMENT}">
										[Date.UTC(${a.userActivityDate.year + 1900},${a.userActivityDate.month},${a.userActivityDate.date}),${(a.userActivityCount?a.userActivityCount:0)}],
									</g:if>
									<% total += (a.userActivityCount?a.userActivityCount:0) %>
								</g:each>	
					         ],
					         name: 'Course Assignment [${total}]'
					      },
					      {
					         type : 'area',
					         data: [
								<% total = 0 %>
								<g:each in="${activities}" var="a">
									<g:if test="${a.userActivityDate && a.userActivityType == UserActivityType.ASSESSMENT}">
										[Date.UTC(${a.userActivityDate.year + 1900},${a.userActivityDate.month},${a.userActivityDate.date}),${(a.userActivityCount?a.userActivityCount:0)}],
									</g:if>
									<% total += (a.userActivityCount?a.userActivityCount:0) %>
								</g:each>	
					         ],
					         name: 'User Assessment [${total}]'
					      },
					      {
					         type : 'area',
					         data: [
								<% total = 0 %>
								<g:each in="${activities}" var="a">
									<g:if test="${a.userActivityDate && a.userActivityType == UserActivityType.SCORM_ASSESSMENT}">
										[Date.UTC(${a.userActivityDate.year + 1900},${a.userActivityDate.month},${a.userActivityDate.date}),${(a.userActivityCount?a.userActivityCount:0)}],
									</g:if>
									<% total += (a.userActivityCount?a.userActivityCount:0) %>
								</g:each>	
					         ],
					         name: 'User SCORM Assessment [${total}]'
					      },
					      {
					         type : 'area',
					         data: [
								<% total = 0 %>
								<g:each in="${activities}" var="a">
									<g:if test="${a.userActivityDate && a.userActivityType == UserActivityType.MARKING_COMPETENT}">
										[Date.UTC(${a.userActivityDate.year + 1900},${a.userActivityDate.month},${a.userActivityDate.date}),${(a.userActivityCount?a.userActivityCount:0)}],
									</g:if>
									<% total += (a.userActivityCount?a.userActivityCount:0) %>
								</g:each>	
					         ],
					         name: 'Assessor Activity [${total}]'
					      }]
			   });
		   });      
		</script>
	
		<g:include view="/include/header.gsp" />			
	
		<!-- end header -->
		<div data-role="content">
			<div class="ui-body">
				<g:datePickerForm controller="user" action="report" id="${params.id}" fromDateValue="${fromDate}" toDateValue="${toDate}" />
				<br />
				<div id="activityChart" style="width:100%;height:400px"></div>
				<p>Note* The date range is non-inclusive of events on ${toDate}. Actual data range is 00:00:00 ${fromDate} to 00:00:00 ${toDate}.</p>	
			</div>
		</div>
		<!--  end content -->
		<g:include view="/include/footer.gsp" />
		</div>
		<!-- end page -->
</body>
</html>