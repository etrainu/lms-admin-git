<html>
<head>
<meta name="layout" content="main" />
<title>User Search</title>
<script>
		        $(function() {
		 
		            $("#callAjax").click(function() {
		                var theName = $.trim($("#userSearchString").val());
		 
		                if(theName.length > 0)
		                {
		                    $.ajax({
		                      type: "GET",
		                      url: "http://localhost:8080/lms-admin/user/userAjaxSearch?type=admin",
		                      data: ({searchString : theName}),
		                      cache: false,
		                      success: function onSuccess(data)
		  		           			 {
				  						alert(data);
				  		                $("#resultLog").html("Result: " + data);
		  		            		 }
		                    });
		                }
		            });

		            $("#userSearchString").keyup(function(event){
		            	  if(event.keyCode == 13)
			              {
		            		  $('#search').submit();
		            	  }
		            });

</script>


</head>

<body>

	<g:form controller="user" action="search" method="post">
		<div data-role="page" id="indexPage">
			<div data-role="header">
				<h1>JQuery Mobile</h1>
			</div>
			<div data-role="content">
				<div data-role="fieldcontain">
					<label for="userSearchString">Please enter your name:</label> <input
						type="text" id="userSearchString" name="userSearchString" value="" />
				</div>
			</div>

			<div data-role="content" data-theme="t">
				<ul data-role="listview" data-theme="c">
					<g:each var="u" in="${users}">
						<li>
							<p>${u.firstname}</p><p>${u.username}</p>
						</li>
					</g:each>
				</ul>
			</div>

			<div data-role="footer">
				<h1>AJAX Demo</h1>
			</div>
		</div>
	</g:form>

</body>
</html>