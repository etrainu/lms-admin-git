<html>
<head>
<title>etrainu mobile admin</title>
<meta name="layout" content="main" />
</head>
<body>
	<div data-role="page" class="type-interior">

		<g:include view="/include/header.gsp" />
		<g:javascript src='editForm.js' />
		<!-- end header -->
		<div data-role="content">
			<g:if test="\${flash.message}">
				<div class="message">
					<p>${flash.message}</p>
				</div>
			</g:if>

			<avatar:gravatar email="${ user.email ? user.email : 'default' }" alt="Avatar" size="120"
				cssClass="block blockCenter" />
				
			<h4>Editing Permissions for ${ user.username }</h4>

			<%-- 
				If you are looking for what causes this to be an ajax call seperate from JQMobile
				Look in editForm.js
			 --%>
			<g:set var="hasPermission" value="true"/>
			<g:if test="${user.permissions == null}">
				<g:set var="hasPermission" value="false"/>
			</g:if>
			 
			<g:form id="editForm" name="editForm" action="updateUserPermissions_ajax" id="${ user.id }" data-ajax="false">

				<div class="ui-form-field">
					<label for="viewUser">View Users and User Reports</label>
					<input id="viewUser" name="permissions" type="checkbox" value="ROLE_VIEW_USER"
						
						<g:if test="${hasPermission && user.permissions.contains('ROLE_VIEW_USER')}">checked="checked"</g:if>
					/>
				</div>

				<div class="ui-form-field">
					<label for="editUser">Edit Users</label>
					<input id="editUser" name="permissions" type="checkbox" value="ROLE_EDIT_USER"
						<g:if test="${hasPermission && user.permissions.contains('ROLE_EDIT_USER')}">checked="checked"</g:if>
					/>
				</div>	

				<div class="ui-form-field">
					<label for="editUserPermissions">Edit User Permissions</label>
					<input id="editUserPermissions" name="permissions" type="checkbox" value="ROLE_EDIT_USER_PERMISSIONS"
						<g:if test="${hasPermission && user.permissions.contains('ROLE_EDIT_USER_PERMISSIONS')}">checked="checked"</g:if>
					/>
				</div>	
				
				<div class="ui-form-field">
					<label for="archiveUser">Archive Users and Reinstate Archived Users</label>
					<input id="archiveUser" name="permissions" type="checkbox" value="ROLE_ARCHIVE_USER"
						<g:if test="${hasPermission && user.permissions.contains('ROLE_ARCHIVE_USER')}">checked="checked"</g:if>
					/>
				</div>	
				
				<div class="ui-form-field">
					<label for="viewCourse">View Courses and Course Reports</label>
					<input id="viewCourse" name="permissions" type="checkbox" value="ROLE_VIEW_COURSE"
						<g:if test="${hasPermission && user.permissions.contains('ROLE_VIEW_COURSE')}">checked="checked"</g:if>
					/>
				</div>
				
				<%-- Not Yet Implemented
				<div class="ui-form-field">
					<label for="editCourse">Edit Courses</label>
					<input id="editCourse" name="permissions" type="checkbox" value="ROLE_EDIT_COURSE"
						<g:if test="${hasPermission && user.permissions.contains('ROLE_EDIT_COURSE')}">checked="checked"</g:if>
					/>
				</div>	
 				--%>
				
				<div class="ui-form-field">
					<label for="archiveCourse">Archive Courses and Reinstate Archived Courses</label>
					<input id="archiveCourse" name="permissions" type="checkbox" value="ROLE_ARCHIVE_COURSE"
						<g:if test="${hasPermission && user.permissions.contains('ROLE_ARCHIVE_COURSE')}">checked="checked"</g:if>
					/>
				</div>
				
				<div class="ui-form-field">
					<label for="viewReferrer">View Referrers and Referrer Reports</label>
					<input id="viewReferrer" name="permissions" type="checkbox" value="ROLE_VIEW_REFERRER"
						<g:if test="${hasPermission && user.permissions.contains('ROLE_VIEW_REFERRER')}">checked="checked"</g:if>
					/>
				</div>

				<div class="ui-form-field">
					<label for="editReferrer">Edit Referrers</label>
					<input id="editReferrer" name="permissions" type="checkbox" value="ROLE_EDIT_REFERRER"
						<g:if test="${hasPermission && user.permissions.contains('ROLE_EDIT_REFERRER')}">checked="checked"</g:if>
					/>
				</div>	
				
				<div class="ui-form-field">
					<label for="archiveReferrer">Archive Referrers and Reinstate Archived Referrers</label>
					<input id="archiveReferrer" name="permissions" type="checkbox" value="ROLE_ARCHIVE_REFERRER"
						<g:if test="${hasPermission && user.permissions.contains('ROLE_ARCHIVE_REFERRER')}">checked="checked"</g:if>
					/>
				</div>
				
				<div class="ui-form-field">
					<label for="salesFigures">View Sales Figures Report</label>
					<input id="salesFigures" name="permissions" type="checkbox" value="ROLE_VIEW_SALES_FIGURES_REPORT"
						<g:if test="${hasPermission && user.permissions.contains('ROLE_VIEW_SALES_FIGURES_REPORT')}">checked="checked"</g:if>
					/>
				</div>

				<button type="submit" name="doEdit" data-theme="b">Save</button>

			</g:form>
		</div>
		<!--  end content -->
		<g:include view="/include/footer.gsp" />
	</div>
	<!-- end page -->
</body>
</html>