<html>
<head>
<title>etrainu mobile admin</title>
<meta name="layout" content="main" />
</head>
<body>
	<div data-role="page" class="type-interior">

		<g:include view="/include/header.gsp" />
		<g:javascript src='editForm.js' />
		<!-- end header -->
		<div data-role="content">
			<g:if test="\${flash.message}">
				<div class="message">
					<p>${flash.message}</p>
				</div>
			</g:if>

			<avatar:gravatar email="${ user.email ? user.email : 'default' }" alt="Avatar" size="120"
				cssClass="block blockCenter" />

			<div id="usernameDiv" class="ui-form-field">
				<label for="username">Username:</label> <input id="username"
					name="username" type="text" class="ui-input-text-disabled"
					value="${ user.username }" disabled="true" />
			</div>

			<%-- 
				If you are looking for what causes this to be an ajax call seperate from JQMobile
				Look in editForm.js
			 --%>
			<g:form id="editForm" name="editForm" action="updateUser_ajax" id="${ user.id }" data-ajax="false">

				<div id="firstnameDiv" class="ui-form-field">
					<label for="firstname">First Name:</label> <input id="firstname"
						name="firstname" type="text" value="${ user.firstname }" />
				</div>

				<div id="lastnameDiv" class="ui-form-field">
					<label for="lastname">Last Name:</label> <input id="lastname"
						name="lastname" type="text" value="${ user.lastname }" />
				</div>

				<div id="emailDiv" class="ui-form-field">
					<label for="email">Address:</label> <input id="email" name="email"
						type="text" value="${ user.email }" />
				</div>

				<div id="cityDiv" class="ui-form-field">
					<label for="city">City:</label> <input id="city" name="city"
						type="text" value="${ user.city }" />
				</div>

				<div id="postcodeDiv" class="ui-form-field">
					<label for="postcode">Postcode:</label> <input id="postcode"
						name="postcode" type="text" value="${ user.postcode }" />
				</div>

				<div id="phoneDiv" class="ui-form-field">
					<label for="phone">Phone:</label> <input id="phone" name="phone"
						type="text" value="${ user.phone }" />
				</div>

				<div id="mobileDiv" class="ui-form-field">
					<label for="mobile">Mobile:</label> <input id="mobile"
						name="mobile" type="text" value="${ user.mobile }" />
				</div>

				<div id="faxDiv" class="ui-form-field">
					<label for="fax">Fax:</label> <input id="fax" name="fax"
						type="text" value="${ user.fax }" />
				</div>

				<div id="dobDiv" class="ui-form-field">
					<label for="dob">Date of Birth:</label>
					<g:if test="${ user.dob }">
						<input id="dob" name="dob" type="text"
							value="${ user.dob.format("dd / MM / yyyy") }" />
					</g:if>
					<g:if test="${ !user.dob }">
						<input id="dob" name="dob" type="text" value="" />
					</g:if>
				</div>

				<button type="submit" name="doEdit" data-theme="b">Save</button>

			</g:form>
		</div>
		<!--  end content -->
		<g:include view="/include/footer.gsp" />
	</div>
	<!-- end page -->
</body>
</html>