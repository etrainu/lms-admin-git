<html>
<head>
<title>etrainu mobile admin</title>

<meta name="layout" content="main" />
</head>
<body>
	<div data-role="page" class="type-interior">

		<!--<g:include view="/include/header.gsp" />-->
		<g:include view="include/navigation.gsp" />

		<script>
			archiveUserUrl = '${createLink(controller:"user",action:"archiveUser_ajax")}';
			unarchiveUserUrl = '${createLink(controller:"user",action:"unarchiveUser_ajax")}';
			groupSearchUrl = "${ createLink( controller:"group", action:"groupAjaxSearch" )}";
		</script>
		<g:javascript src='user.js' />
		<!-- end header -->
		<div data-role="content">
			<div class="ui-body">
				<g:form controller="user" action="user" data-ajax="false">
					<fieldset class="ui-grid-b">
						<div class="ui-block-a" data-theme="c">
							<!--  user type -->
							<label for="userType">User Type:</label> <select name="userType"
								id="userType" data-theme="c">
								<option value="admin"
									${(params.userType=="admin")?"selected='selected'":""}>Admin</option>
								<option value="B2B"
									${(params.userType=="B2B")?"selected='selected'":""}>B2B</option>
								<option value="B2C"
									${(params.userType=="B2C")?"selected='selected'":""}>B2C</option>
							</select>
							<!-- end user type -->
						</div>
						<!-- end block a -->

						<div class="ui-block-b">
							<!-- org list -->
							<label for="groupOrg" class="select">Org:</label> <select
								name="groupOrg" id="groupOrg"
								onChange="updateSubGroups('groupOrg','groupDept');"
								data-theme="c">
								<option value=""></option>
								<option value="">All</option>
								<g:each in="${groups}" var="group">
									<% selectedStr = ""
										if( params.groupOrg && Integer.parseInt(params.groupOrg) == Integer.parseInt(group.id) ) { 
											selectedStr = 'selected="selected"' 
										}
									%>
									<option value="${group.id}" ${selectedStr}>
										${group.groupName}
									</option>
								</g:each>
							</select>
							<!--  end org list -->
						</div>
						<!-- end block b -->
						
						<div class="ui-block-c">
							<!-- dep list -->
							<label for="groupDept" class="select">Dep:</label> <select
								name="groupDept" id="groupDept" data-theme="c"
								onChange="deptTextFix()" onClick="deptTextFix()">
								<option value=""></option>
								<option value="">All</option>
							</select>

							<script>
									//preload departments where applicable, select the correct one
									if( $ && $('#groupDept').length && String("${ params.groupOrg }").length ) {
										groupDept = String("${ params.groupDept}");
										if( groupDept.length && groupDept != 'all'  ) {
											updateSubGroups('groupOrg','groupDept', groupDept);
										} else {
											updateSubGroups('groupOrg','groupDept');
										}
										
									}
								</script>
							<!--  end dep list -->
						</div>
						<!-- end block c -->
					</fieldset>
					<!-- end grid a -->
					<div class="user-search">
						<input type="search" name="searchString" class="ui-body ui-body-d"
							id="name" value="${ params.searchString }"
							placeholder="User details..." />
							
							<input type="checkbox" name="archived" id="archived_on" value="true" ${(params.archived.equals('true'))?'checked="checked"':''} />
						<label for="archived_on">Arch</label>
						
						<input type="submit" id="searchButton" value="Search"
							data-icon="search" data-inline="true" />
					</div>
				</g:form>
			</div>
			<!--  end form -->
			<g:if test="${ users.size() < totalUsers }">
				<h5>
					Your search returned
					${totalUsers}
					results or more but only
					${users.size()}
					results were displayed.<br /> Consider refining your search
					criteria.
				</h5>
			</g:if>

			<% 
				//This is so we don't have to keep creating new objects for the current date/time
				now = new Date() 
			%>
			<ul data-role="context-listview" data-inset="true">
				<g:each var="u" in="${users}">
					<%
							percentComplete = 0;
							if( u.completedCourses && u.courseCount ) {
								percentComplete = Math.round( 100 * (u.completedCourses / u.courseCount) );
							}
							
							//Allow date to be shown when its not null
							//But wrap it with the class if its more than a year old
							loginDefault = 'Never';
							loginTest = true;
							if( u.lastLogin ) {
								loginTest = false;
								loginDefault = u.lastLogin.format( "dd / MM / yyyy" )
								dayDiff = Math.ceil( ( now.getTime() - u.lastLogin.getTime() ) / (8.64 * 10000000) )
								if( dayDiff > 365 ) {
									loginTest = true;
								}
							}
							
							theme = ( u.archiveDate ) ? 'r' : '' ;
						%>
					<li data-theme="${theme}" data-userid="${ u.id }"
						data-archived="${u.archiveDate!=null}"><g:link
							controller="user" action="report" params="[id:u.id]"
							rel="external">
							<div>
								${u.firstname}
								${u.lastname}
							</div>
							<div class="user-details-left">
								${ u.username }
								:
								<g:defaultText value="${u.email}" default="No Email"
									defaultBlank="true" defaultClass="error" />
								(
								${ userType }
								)<br />
								Balance: $${ u.balance }<br />
								<g:if test="${u.archiveDate}">
									Archived: <g:formatDate format="dd / MM / yyyy"
										date="${ u.archiveDate }" />
								</g:if>
								<g:else>
									Created: <g:formatDate format="dd / MM / yyyy"
										date="${ u.dateCreated }" />
								</g:else>
								<br /> Last Login:
								<g:defaultText value="${u.lastLogin}" default="${loginDefault}"
									defaultClass="error" format="dd / MM / yyyy"
									test="${loginTest}" />
								<br /> Location:
								<g:defaultText value="${u.city}," default="" defaultBlank="true"
									defaultClass="error" test="${!u.city}" />
								<g:defaultText value="${u.state}" default="" defaultBlank="true"
									defaultClass="error" />
							</div>
							<p class="ui-li-aside">
								<g:if test="${ userType != 'Admin' }">
										Course Count: ${ u.courseCount }
									(<g:defaultText value="${percentComplete}%"
										acceptedClass="valid" defaultClass="error"
										test="${percentComplete<80}" /> completed)
									<br />
									First Course Assigned: <g:formatDate format="dd / MM / yyyy"
										date="${ u.firstCourse }" />
									<br />
									Most Recent Course: <g:formatDate format="dd / MM / yyyy"
										date="${ u.mostRecentCourse }" />
									<br />
								</g:if>
								${ u.hierarchy }
							</p>
						</g:link>

						<div data-role="menu" data-icon="gear">
							<g:link controller="user" action="report" params="[id:u.id]"
								rel="external">Activity Report</g:link>
							<sec:ifAnyGranted roles="ROLE_EDIT_USER">
								<g:link controller="user" action="editUser" params="[id:u.id]"
									rel="external">Edit User</g:link>
							</sec:ifAnyGranted>
							<sec:ifAnyGranted roles="ROLE_EDIT_USER_PERMISSIONS">
								<g:link controller="user" action="editPermissions" params="[id:u.id]"
									rel="external">Edit Permissions</g:link>
							</sec:ifAnyGranted>
							<sec:ifAnyGranted roles="ROLE_ARCHIVE_USER">
								<a href="#" class="archiveUser">Archive User</a>
								<a href="#" class="unarchiveUser">Reinstate User</a>
							</sec:ifAnyGranted>
						</div></li>

				</g:each>
			</ul>
		</div>
		<!--  end content -->
		<g:include view="/include/footer.gsp" />
	</div>
	<!-- end page -->
</body>
</html>