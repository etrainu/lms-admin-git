package lms.admin

import grails.converters.JSON
import com.etrainu.shared.service.GroupService

class GroupController {
	
	GroupService groupService
	
	def groupAjaxSearch =
	{
		def json = new JSON(groupService.getGroups(params.gid))
		render ( text:json.toString(), contentType:"text/javascript", encoding:"UTF-8" )			
	}
}
