package com.etrainu.admin.course

import grails.plugins.springsecurity.Secured
import groovy.lang.Closure
import groovy.util.ConfigObject

import java.text.SimpleDateFormat

import org.codehaus.groovy.grails.commons.ConfigurationHolder
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils

import com.etrainu.course.Course
import com.etrainu.shared.service.CourseService
import com.etrainu.utils.JSONResult
import com.etrainu.utils.SearchResult

class CourseController {
	
	def springSecurityService
	CourseService courseService
	
	static final private String NO_ID_SPECIFIED = "You must specify the id(?id=) of the course."
	static final private String NOT_FOUND = "Could not find course"
	
	// TODO: Change this back to grailsApplications.config when grails 2.0 is used
	// as currently, grailsApplication cannot be used when unit testing controllers
	final private ConfigObject config = ConfigurationHolder.config
	
	@Secured(['ROLE_VIEW_COURSE'])
    def search = {
		final def results = [:]
		final List courses = []
		Integer totalCourses = 0
		
		switch(request.method)
		{
			case "POST":
				def SearchResult searchResult = courseService.searchCourses(params.searchString, params.archived)
				courses = searchResult.results
				totalCourses = searchResult.maxResults
			break
		}
		[courses: courses, totalCourses:totalCourses]
    }
	
	@Secured(['ROLE_VIEW_COURSE'])
	def report =
	{
		withCourse{course ->
			
			//Setting Dates Defaults
			def df = new SimpleDateFormat("dd-MM-yyyy");
			
			def toDate
			def fromDate
			def cal = Calendar.getInstance()
			if( !params.toDate ) {
				toDate = df.format(cal.getTime())
			} else {
				toDate = params.toDate
			}
			
			if( !params.fromDate ) {
				cal.add( Calendar.MONTH, -1 )
				fromDate = df.format(cal.getTime())
			} else {
				fromDate = params.fromDate
			}
			
			//Requesting activities from server
			[
				activities: courseService.getActivity(course.id, fromDate, toDate), 
				course:course, title: "Activity Report - " + course.name, 
				fromDate: fromDate, 
				toDate: toDate
			]
		}
	}
	
	def archiveCourse_ajax =
	{

		//Rendering will occur in the security check
		if( !checkSecurityForAjax() ) { return }

		withCourse(true){course ->
			courseService.archiveCourse(course.id)
			log.info springSecurityService.getPrincipal().username + ' archived course ' + course.name + ' (' + course.id + ')' 
			render( new JSONResult( 'Course successfully archived.' , true).toJSON() )
		}
	}

	def unarchiveCourse_ajax =
	{
		if( !checkSecurityForAjax() ) { return }
				
		withCourse(true){ course ->
			courseService.unarchiveCourse(course.id)
			log.info springSecurityService.getPrincipal().username + ' unarchived course ' + course.name + ' (' + course.id + ')'
			render( new JSONResult( 'Course successfully reinstated.' , true).toJSON() )
		}
	}
	
	private def withCourse(id="id", ajax=false, Closure c) {
		if( !params.id ) {
			if (ajax) {
				render( new JSONResult( NO_ID_SPECIFIED , false).toJSON() )
			}
			else {
				flash.message = NO_ID_SPECIFIED
				redirect (action:"search")
			}
		}

		final Course course = courseService.getCourse(params.id)
		if(course) {
			c.call course
		} else {
			if (ajax) {
				render( new JSONResult(NOT_FOUND + "[${params[id]}]", false).toJSON() )
			}
			else {
				flash.message = NOT_FOUND + "[${params[id]}]"
				redirect (action:"search")
			}
		}
	}
	
	//This method cannot terminate the request, only return back to the caller
	private checkSecurityForAjax() {
		if( ! SpringSecurityUtils.ifAnyGranted('ROLE_ARCHIVE_COURSE') ) {
			render( new JSONResult("Access Denied.", false).toJSON() )
			return false
		}
		return true
	}
}
