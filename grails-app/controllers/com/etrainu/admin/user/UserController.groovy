package com.etrainu.admin.user

import grails.plugins.springsecurity.Secured
import groovy.lang.Closure

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Calendar

import org.codehaus.groovy.grails.commons.ConfigurationHolder
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils

import com.etrainu.shared.service.GroupService
import com.etrainu.shared.service.UserService
import com.etrainu.user.User
import com.etrainu.user.UserDetails
import com.etrainu.utils.APIClient
import com.etrainu.utils.JSONResult
import com.etrainu.utils.SearchResult

class UserController {
	
	def springSecurityService
	def groupService
	def userService
	def securityService
	
	static final private String NO_ID_SPECIFIED = "You must specify the id(?id=) of the user."
	static final private String NOT_FOUND = "Could not find user"
	
	// TODO: Change this back to grailsApplications.config when grails 2.0 is used
	// as currently, grailsApplication cannot be used when unit testing controllers
	final private ConfigObject config = ConfigurationHolder.config
	
	@Secured(['ROLE_VIEW_USER'])
	def index = {
		redirect(action:"user")
	}
	
	@Secured(['ROLE_VIEW_USER'])
	def user = {
		final def users = []
		final def userType
		final def totalUsers = 0
		final def SearchResult searchResults
		
		switch(request.method)
		{
			case "POST":
				def groupId = null
				def maxResults = config.api.maxResultsDisplayed
				
				if( params.groupDept ) {
					groupId = params.groupDept
					maxResults = config.api.maxResultsDisplayedForDepartement
				} else if (params.groupOrg) {
					groupId = params.groupOrg
				}
				
				searchResults = userService.searchUsers(
					params.searchString,
					params.userType,
					params.archived,
					groupId,
					maxResults
				)
				
				users = searchResults.results
				totalUsers = searchResults.maxResults
				
				//since usertype is going to be fixed based on the data above, derive it from that
				if( params.userType == 'admin' ) {
					userType = "Admin"
				} else {
					userType = "Participant - " + params.userType
				}
				
				break
		}

		return [totalUsers:totalUsers, users:users, groups: groupService.getGroups(), userType:userType]
	}
	
	@Secured(['ROLE_EDIT_USER'])
	def editUser = {
		cache false
		
		withUser{ user ->
			[user:user]
		}
	}
	
	@Secured(['ROLE_EDIT_USER'])
	def updateUser_ajax = 
	{
		def updateProperties = params
		//Manipulate the dob data into a proper date object
		def dobString
		if( params.dob.length() ) {
			DateFormat df = new SimpleDateFormat("dd / M / yyyy")
			String dobData = params.dob
			
			params.dob = df.parse( dobData )
			updateProperties.dob = params.dob.format("yyyy-M-dd")
		} else {
			updateProperties.dob = ''
		}
		
		userService.updateUser updateProperties

		render( new JSONResult( 'Profile Updated.' , true).toJSON() )
	}
	
	@Secured(['ROLE_EDIT_USER_PERMISSIONS'])
	def updateUserPermissions_ajax = {
		withUser(params.id, true){ user ->
			def permissions = '';
			params.permissions.each {
				// TODO: Figure out a way to make sure permissions returns an array or a string and then simplify this
				if( it.contains("ROLE_") )
					permissions += it + ','
			}
			
			if( permissions.size() == 0 ) permissions = params.permissions
			
			userService.updateUserPermissions params.id, permissions
			
			log.info springSecurityService.getPrincipal().username + ' updated permissions for ' + user.username + ' (' + user.id + ') ' + permissions
			
			render( new JSONResult( 'Permissions Updated.', true).toJSON() )
		}
	}
	
	def archiveUser_ajax =
	{
		if( !checkSecurityForAjax() ) { return }
		
		withUser(params.id, true){ user ->
			userService.archiveUser( user.id )
			log.info springSecurityService.getPrincipal().username + ' archived user ' + user.username + ' (' + user.id + ')'
			
			//TODO: add better handling, add error handling
			render( new JSONResult( 'User successfully archived.' , true).toJSON() )
		}
	}
	
	
	def unarchiveUser_ajax =
	{
		if( !checkSecurityForAjax() ) { return }
		withUser(params.id, true){ user ->
			userService.unarchiveUser(user.id)
			log.info springSecurityService.getPrincipal().username + ' unarchived user ' + user.username + ' (' + user.id + ')'
			
			//TODO: add better handling, add error handling
			render( new JSONResult( 'User successfully reinstated.' , true).toJSON() )
		}
	}
	
	@Secured(['ROLE_VIEW_USER'])
	def report = 
	{
		withUser{ user -> 
			
			//Setting Dates Defaults
			def df = new SimpleDateFormat("dd-MM-yyyy");
			
			def toDate
			def fromDate
			def cal = Calendar.getInstance()
			if( !params.toDate ) {
				toDate = df.format(cal.getTime())
			} else {
				toDate = params.toDate
			}
			
			if( !params.fromDate ) {
				cal.add( Calendar.MONTH, -1 )
				fromDate = df.format(cal.getTime())
			} else {
				fromDate = params.fromDate
			}
			
			//Requesting activity from server through REST API
			def activities = userService.getUserActivity(user.id, fromDate, toDate)

			[
				activities : activities, 
				user:user, 
				title: "Activity Report - " + user.username, 
				fromDate: fromDate, 
				toDate: toDate
			]
		}
	}

	@Secured(['ROLE_VIEW_USER'])
	def partnerships = {
		withUser{ user ->
			def partnerships = userService.getPartnerships(user.id)
			[partnerships : partnerships]
		}
	}
	
	@Secured(['ROLE_VIEW_USER'])
	def search =
	{
		switch(request.method)
		{
			case "POST":
			//request Assessment activity
				def userResponse = new APIClient(config.userSearchApiString,[type:params.type,searchString:params.searchString], securityService.getCredentials()).getXml()

				final def xml = userResponse.data
				final def users = []
				xml.user.each {
					def userDetails = UserDetails.parseXML(it)
					users.add userDetails
				}
				[users:users]
				break
		}
	}
	
	def editPermissions =
	{
		withUser { user ->
			[user:user]	
		}
	}
	
	//This method cannot terminate the request, only return back to the caller
	private checkSecurityForAjax() {
		if( ! SpringSecurityUtils.ifAnyGranted('ROLE_ARCHIVE_USER') ) {
			render( new JSONResult("Access Denied.", false).toJSON() )
			return false
		}
		return true
	}
	
	private def withUser(id="id", ajax=false, Closure c) {

		if( !params.id ) {
			if (ajax) {
				render( new JSONResult( NO_ID_SPECIFIED , false).toJSON() )
			}
			else {
				flash.message = NO_ID_SPECIFIED
				redirect (action:"user")
			}
		}
		
		final User user = userService.getUser(params.id)
		if(user) {
			c.call user
		} else {
			if (ajax) {
				render( new JSONResult(NOT_FOUND + "[${params.id}]", false).toJSON() )
			}
			else {
				flash.message = NOT_FOUND + "[${params.id}]"
				redirect (action:"user")
			}
		}
	}
}
