package com.etrainu.admin.sales

import java.text.SimpleDateFormat;

import grails.plugins.springsecurity.Secured
import groovy.lang.Closure
import groovy.util.ConfigObject

import org.codehaus.groovy.grails.commons.ConfigurationHolder
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils

import com.etrainu.course.Course
import com.etrainu.shared.service.CourseService
import com.etrainu.shared.service.SalesService;
import com.etrainu.utils.JSONResult
import com.etrainu.utils.SearchResult

class SalesController {
	
	def springSecurityService
	SalesService salesService
	
	static final private String NO_ID_SPECIFIED = "You must specify the id(?id=) of the course."
	static final private String NOT_FOUND = "Could not find course"
	
	// TODO: Change this back to grailsApplications.config when grails 2.0 is used
	// as currently, grailsApplication cannot be used when unit testing controllers
	final private ConfigObject config = ConfigurationHolder.config
	
	
	@Secured(['ROLE_VIEW_SALES_FIGURES_REPORT'])
	def report =
	{
		
		//Setting Dates Defaults
		def df = new SimpleDateFormat("dd-MM-yyyy");
		
		def toDate
		def fromDate
		def cal = Calendar.getInstance()
		if( !params.toDate ) {
			toDate = df.format(cal.getTime())
		} else {
			toDate = params.toDate
		}
		
		if( !params.fromDate ) {
			cal.add( Calendar.MONTH, -1 )
			fromDate = df.format(cal.getTime())
		} else {
			fromDate = params.fromDate
		}
		
		def salesActivityMap = salesService.getActivity(fromDate,toDate)
		
		[
			activityMap : salesActivityMap,
			title: "Sales Report",
			fromDate: fromDate,
			toDate: toDate
		]
		
	}

}
