package com.etrainu.admin.referrer

import grails.plugins.springsecurity.Secured
import groovy.lang.Closure
import groovy.util.ConfigObject

import java.text.SimpleDateFormat

import org.codehaus.groovy.grails.commons.ConfigurationHolder
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils

import com.etrainu.referrer.Referrer
import com.etrainu.shared.service.ReferrerService
import com.etrainu.utils.JSONResult

class ReferrerController {

	def springSecurityService
	ReferrerService referrerService

	static final private String NO_ID_SPECIFIED = "You must specify the id(?id=) of the referrer."
	static final private String NOT_FOUND = "Could not find referrer"

	final private ConfigObject config = ConfigurationHolder.config

	@Secured(['ROLE_VIEW_REFERRER'])
	def search = {
		final List referrers = []
		final List searchResult = []
		Integer totalReferrers = 0

		switch(request.method) {
			case "POST":

				referrers = referrerService.searchReferrers(params.name, params.archived)

				totalReferrers = referrers.size
				break
		}

		[referrers: referrers, totalReferrers: totalReferrers]
	}

	@Secured(['ROLE_VIEW_REFERRER'])
	def report = {
		withReferrer{referrer ->
			
			//Setting Dates Defaults
			def df = new SimpleDateFormat("dd-MM-yyyy");
			
			def toDate
			def fromDate
			def cal = Calendar.getInstance()
			if( !params.toDate ) {
				toDate = df.format(cal.getTime())
			} else {
				toDate = params.toDate
			}
			
			if( !params.fromDate ) {
				cal.add( Calendar.MONTH, -1 )
				fromDate = df.format(cal.getTime())
			} else {
				fromDate = params.fromDate
			}
			
			[
				activities: referrerService.getSalesActivity(referrer.id, fromDate, toDate) , 
				referrer:referrer, 
				title: "Activity Report - " + referrer.name, 
				fromDate: fromDate, 
				toDate: toDate
			]
		}
	}

	@Secured(['ROLE_EDIT_REFERRER'])
	def editReferrer = {
		//should never cache this page as the data needs to be up to the second every time
		cache false 

		if( !params.id )
		{
			//No id provided - New Referrer
			[referrer:new Referrer(isDiscount:false, isPercentage:false)]
		}
		else
		{
			withReferrer{ referrer ->
				[referrer:referrer]
			}
		}
	}

	@Secured(['ROLE_EDIT_REFERRER'])
	def updateReferrer_ajax = {
		if( !checkSecurityForAjax() ) {
			return
		}
		
		def Referrer referrer = referrerService.getReferrer(params.id)
		if(referrer) {
			referrer.update( params )
		} else {
			referrer = Referrer.parse( params )
		}
		
		def validationResponse = referrer.validate()

		if( validationResponse.hasErrors ) {
			return render (new JSONResult( validationResponse.errorMessage , false).toJSON() )
		}

		referrerService.updateReferrer referrer
			
		log.info springSecurityService.getPrincipal().username + ' edited referrer ' + referrer.name + ' (' + referrer.name + ')'

		render( new JSONResult( 'Referrer Updated.' , true).toJSON() )
	}

	def archiveReferrer_ajax = {
		if( !checkSecurityForAjax() ) {
			return
		}

		withReferrer(params.id,true){ referrer ->
			referrerService.archiveReferrer( referrer.id )
			log.info springSecurityService.getPrincipal().username + ' archived referrer ' + referrer.name + ' (' + referrer.name + ')'

			//TODO: add better handling, add error handling
			render( new JSONResult( 'Referrer successfully archived.' , true).toJSON() )
		}
	}


	def unarchiveReferrer_ajax = {
		if( !checkSecurityForAjax() ) {
			return
		}

		withReferrer(params.id,true){ referrer ->
			referrerService.unarchiveReferrer(referrer.id)
			log.info springSecurityService.getPrincipal().username + ' Unarchived referrer ' + referrer.name + ' (' + referrer.name + ')'

			//TODO: add better handling, add error handling
			render( new JSONResult( 'Referrer successfully reinstated.' , true).toJSON() )
		}
	}

	private def withReferrer(id="id", ajax=false, Closure c) {
		if( !params.id ) {
			if (ajax) {
				render( new JSONResult( NO_ID_SPECIFIED , false).toJSON() )
			}
			else {
				//flash.message = NO_ID_SPECIFIED
				//redirect (action:"search")
				flash.message = "New Referrer"
			}
		}

		final Referrer referrer = referrerService.getReferrer(params.id)
		if(referrer) {
			c.call referrer
		} else {
			if (ajax) {
				render( new JSONResult(NOT_FOUND + "[${params[id]}]", false).toJSON() )
			}
			else {
				flash.message = NOT_FOUND + "[${params[id]}]"
				//redirect (action:"search")
			}
		}
	}

	//This method cannot terminate the request, only return back to the caller
	private checkSecurityForAjax() {
		if( ! SpringSecurityUtils.ifAnyGranted('ROLE_EDIT_REFERRER') ) {
			render( new JSONResult("Access Denied.", false).toJSON() )
			return false
		}
		return true
	}
}
