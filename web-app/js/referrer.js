$(function() {
	$('.archiveReferrer').each(function() {
		var self = this
			, jSelf = $(self)
			, menuPage = self.menuPage
			, item = self.item
		;
		
		//if menuPage.length is 0, then its the mobiled object, which is not what we want to operate on (to prevent multiple binding)
		if( menuPage.length == 0 ) return;
		
		var referrerid = item.jqmData("referrerid")
			, archived = item.jqmData("archived")
			, unarchiveButton = menuPage.find('.unarchiveReferrer')
		;
		
		if( archived ) {
			jSelf.closest('li').css('display','none');
		}
		
		jSelf.bind('vclick',function() {
			$.ajax({
				url: archiveReferrerUrl + "?id=" + encodeURIComponent(referrerid),
				dataType: "JSON",
				success: function(resp) {
					alert( resp.message );
					if( resp.success ) {
						switchItemTheme(item,'c','r');
					
						unarchiveButton.closest('li').css('display','block');
						jSelf.closest('li').css('display','none');
					}
				}
			});
		});
	});
	
	$('.unarchiveReferrer').each(function() {
		var self = this
			, jSelf = $(self)
			, menuPage = self.menuPage
			, item = self.item
		;
		
		//if menuPage.length is 0, then its the mobiled object, which is not what we want to operate on (to prevent multiple binding)
		if( menuPage.length == 0 ) return;
		
		var referrerid = item.jqmData("referrerid")
			, archived = item.jqmData("archived")
			, archiveButton = menuPage.find('.archiveReferrer')
		;
		
		if( !archived ) {
			jSelf.closest('li').css('display','none');
		}
		
		jSelf.bind('vclick',function() {
			$.ajax({
				url: unarchiveReferrerUrl + "?id=" + encodeURIComponent(referrerid),
				dataType: "JSON",
				success: function(resp) {
					alert( resp.message );
					if( resp.success ) {
						switchItemTheme(item,'r','c');
					
						archiveButton.closest('li').css('display','block');
						jSelf.closest('li').css('display','none');
					}
				}
			});
		});
	});
});

function switchItemTheme(jObject, oldTheme, newTheme) {
	jObject
		.removeClass("ui-btn-up-"+oldTheme)
		.addClass("ui-btn-up-"+newTheme)
		.attr("data-theme",newTheme)
		.jqmData("theme",newTheme)
		.find('div.ui-btn-up-'+oldTheme)
		.removeClass("ui-btn-up-"+oldTheme)
		.addClass("ui-btn-up-"+newTheme)
		.attr("data-theme",newTheme)
		.jqmData("theme",newTheme)
	;
}