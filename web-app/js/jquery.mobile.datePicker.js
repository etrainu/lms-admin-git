(function($, undefined ) {
	$( ".ui-page" ).live( "pagecreate", function(){		
		$( "input[type='date'], input:jqmData(type='date')" ).each(function(){
			$(this).after( $( "<div />" ) ).datepicker({
				showOtherMonths: true,
				altField: "#" + $(this).attr( "id" ),
				duration: 0,
				showWeek: false,
				showAnim: '', /*remove animation*/
				dateFormat: 'dd-mm-yy'
			});
		});
	});
})( jQuery );