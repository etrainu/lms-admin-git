// Add the load popup to all external links
// Ajax links already have it added by jq mobile
$( document ).bind( "pagecreate create", function( e ) {
	$('a[rel="external"]:not( :jqmData(showPageLoad="true") )').each(function() {
		var self = this,
			$self = $(self)
		;
		
		$self.attr('data-showPageLoad',true);
		$self.click(function() {
			$.mobile.showPageLoadingMsg();
		});
	});
	
	$('form:jqmData(ajax="false"):not( :jqmData(showPageLoad="true") )').each(function() {
		var self = this,
			$self = $(self)
		;
		
		$self.attr('data-showPageLoad',true);
		$self.bind('submit', function() {
			$.mobile.showPageLoadingMsg();
		});
	})
});