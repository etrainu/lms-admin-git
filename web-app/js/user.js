//needs the jqstart since its not an ajax page
$(function() {
	debug = false;
	
	$('.archiveUser').each(function() {
		
		var self = this
			, jSelf = $(self)
			, menuPage = self.menuPage
			, item = self.item
		;
		
		//if menuPage.length is 0, then its the mobiled object, which is not what we want to operate on (to prevent multiple binding)
		if( menuPage.length == 0 ) return;

		var userid = item.jqmData("userid")
			, archived = item.jqmData("archived")
			, unarchiveButton = menuPage.find('.unarchiveUser')
		;
		
		if( archived ) {
			jSelf.closest('li').css('display','none');
		}
		
		jSelf.bind('vclick',function() {
			if( debug ) {
				switchItemTheme(item,'c','r');
				unarchiveButton.closest('li').css('display','block');
				jSelf.closest('li').css('display','none');
			} else {
				$.ajax({
					url: archiveUserUrl + "?id=" + userid,
					dataType: "JSON",
					success: function(resp) {
						alert( resp.message );
						if( resp.success ) {
							switchItemTheme(item,'c','r');
						
							unarchiveButton.closest('li').css('display','block');
							jSelf.closest('li').css('display','none');
						}
					}
				});
			}
		});
	});
	
	$('.unarchiveUser').each(function() {
		var self = this
			, jSelf = $(self)
			, menuPage = self.menuPage
			, item = self.item
		;
		
		//if menuPage.length is 0, then its the mobiled object, which is not what we want to operate on (to prevent multiple binding)
		if( menuPage.length == 0 ) return;
		
		var userid = item.jqmData("userid")
			, archived = item.jqmData("archived")
			, archiveButton = menuPage.find('.archiveUser')
		;
		
		if( !archived ) {
			jSelf.closest('li').css('display','none');
		}
		
		jSelf.bind('vclick',function() {
			if( debug ) {
				switchItemTheme(item,'r','c');
				archiveButton.closest('li').css('display','block');
				jSelf.closest('li').css('display','none');
			} else {
				$.ajax({
					url: unarchiveUserUrl + "?id=" + userid,
					dataType: "JSON",
					success: function(resp) {
						alert( resp.message );
						if( resp.success ) {
							switchItemTheme(item,'r','c');
						
							archiveButton.closest('li').css('display','block');
							jSelf.closest('li').css('display','none');
						}
					}
				});
			}
		});
	});
});

function switchItemTheme(jObject, oldTheme, newTheme) {
	jObject
		.removeClass("ui-btn-up-"+oldTheme)
		.addClass("ui-btn-up-"+newTheme)
		.attr("data-theme",newTheme)
		.jqmData("theme",newTheme)
		.find('div.ui-btn-up-'+oldTheme)
		.removeClass("ui-btn-up-"+oldTheme)
		.addClass("ui-btn-up-"+newTheme)
		.attr("data-theme",newTheme)
		.jqmData("theme",newTheme)
	;
}

function updateSubGroups(parentGroup, childGroup, preselect) {
	var org = $.trim($("#" + parentGroup).val());
	
    if(org.length > 0)
    {
    	$.mobile.showPageLoadingMsg();
    	
        $.ajax({
          type: "GET",
          url: groupSearchUrl,
          data: ({gid : org}),
          cache: false,
          dataType: "JSON",
          success: function onSuccess(data)
   			 {
        	  	$.mobile.hidePageLoadingMsg();
				var dept = $("#" + childGroup);
				var opt = '';
				dept.get(0).options.length = 2;
				$.each(data, function(index, value) {
					opt = $("<option />")
							.val(value.id)
							.text(value.groupName);
					
					dept.append( opt );
					
					if( preselect && parseInt(preselect) == parseInt(value.id) ) {
						opt.attr('selected','selected');
						dept.parent().find("span > .ui-btn-text:first").text( value.groupName );
					}
					
					dept.trigger('change');
				});		
    		 }
        });
    } else {
    	var dept = $("#" + childGroup);
    	dept.get(0).options.length = 2;
    	dept.get(0).selectedIndex = 0;
    	dept.trigger('change');
    }
}

function deptTextFix() {
	// the latest jquery mobile doesn't want to play ball so this function will manually update the display text
	// Unknown issue but it doesn't seem to make much sense
	var dept = $("#groupDept");
	var idx = dept[0].selectedIndex;
	var options = dept[0].options;
	var selectedOpt = options[idx];
	var selectedText = selectedOpt.innerHTML;

	dept.parent().find('.ui-btn-text').html(selectedOpt.innerHTML);
}