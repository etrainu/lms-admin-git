(function( $, undefined ) {
var listCountPerPage = {};

$.widget( "mobile.contextListview", $.mobile.widget, {
	options: {
		theme: "c",
		countTheme: "c",
		headerTheme: "b",
		dividerTheme: "b",
		splitIcon: "arrow-r",
		splitTheme: "b",
		inset: false,
		initSelector: ":jqmData(role='context-listview')"
	},

	_create: function() {
		var t = this;

		t.element.addClass(function( i, orig ) {
			return orig + " ui-listview " + ( t.options.inset ? " ui-listview-inset ui-corner-all ui-shadow " : "" );
		});

		t.refresh();
	},

	_itemApply: function( $list, item ) {
		// TODO class has to be defined in markup
		item.find( ".ui-li-count" )
			.addClass( "ui-btn-up-" + ( $list.jqmData( "counttheme" ) || this.options.countTheme ) + " ui-btn-corner-all" ).end()
		.find( "h1, h2, h3, h4, h5, h6" ).addClass( "ui-li-heading" ).end()
		.find( "p, dl" ).addClass( "ui-li-desc" ).end()
		.find( ">img:eq(0), .ui-link-inherit>img:eq(0)" ).addClass( "ui-li-thumb" ).each(function() {
			item.addClass( $(this).is( ".ui-li-icon" ) ? "ui-li-has-icon" : "ui-li-has-thumb" );
		}).end()
		.find( ".ui-li-aside" ).each(function() {
			var $this = $(this);
			$this.prependTo( $this.parent() ); //shift aside to front for css float
		});
	},

	_removeCorners: function( li, which ) {
		var top = "ui-corner-top ui-corner-tr ui-corner-tl",
			bot = "ui-corner-bottom ui-corner-br ui-corner-bl";

		li = li.add( li.find( ".ui-btn-inner, .ui-li-link-alt, .ui-li-thumb" ) );

		if ( which === "top" ) {
			li.removeClass( top );
		} else if ( which === "bottom" ) {
			li.removeClass( bot );
		} else {
			li.removeClass( top + " " + bot );
		}
	},

	refresh: function( create ) {
		this.parentPage = this.element.closest( ".ui-page" );
		this._createSubPages();

		var o = this.options,
			$list = this.element,
			self = this,
			dividertheme = $list.jqmData( "dividertheme" ) || o.dividerTheme,
			listsplittheme = $list.jqmData( "splittheme" ),
			listspliticon = $list.jqmData( "spliticon" ),
			li = $list.children( "li" ),
			counter = $.support.cssPseudoElement || !$.nodeName( $list[ 0 ], "ol" ) ? 0 : 1,
			item, itemClass, itemTheme,
			a, last, splittheme, countParent, icon;

		if ( counter ) {
			$list.find( ".ui-li-dec" ).remove();
		}

		for ( var pos = 0, numli = li.length; pos < numli; pos++ ) {
			item = li.eq( pos );
			itemClass = "ui-li";

			// If we're creating the element, we update it regardless
			if ( create || !item.hasClass( "ui-li" ) ) {
				itemTheme = item.jqmData("theme") || o.theme;
				a = item.children( 'a,:jqmData(role="menu")' );
				
				if ( a.length ) {
					icon = item.jqmData("icon");

					item.buttonMarkup({
						wrapperEls: "div",
						shadow: false,
						corners: false,
						iconpos: "right",
						icon: a.length > 1 || icon === false ? false : icon || "arrow-r",
						theme: itemTheme
					});

					a.first().addClass( "ui-link-inherit" );

					if ( a.length > 1 ) {
						itemClass += " ui-li-has-alt";
						last = a.last();
						lastTag = a.get(-1).tagName;
						splittheme = listsplittheme || last.jqmData( "theme" ) || o.splitTheme;
						/*
						//Extract all the items out of last, turn it into one of those fancy dialog things as seen on selectmenu
						*/
						var thisPage = last.closest(".ui-page")
							, menuPage = $( '<div class="ui-selectmenu ui-overlay-shadow ui-corner-all pop ui-body-a ui-selectmenu-hidden"></div>' )
								.appendTo( $.mobile.pageContainer )
							, menu = $( '<ul class="ui-selectmenu-list ui-listview"" role="listbox" aria-labelledby="c-button" data-theme="c"></ul>' )
								.appendTo( menuPage )
							, screen = $( "<div>", {"class": "ui-selectmenu-screen ui-screen-hidden"})
								.appendTo( thisPage )
						;
						
						last.children("a").each(function() {
							jThis = $(this);
							
							$.extend(jThis[0], {
								item:item,
								menuPage:menuPage
							});
							
							var elem = $('<li class="ui-btn ui-btn-icon-right ui-li ui-btn-up-c" data-theme="c">'
								+ '<div class="ui-btn-inner ui-li"><div class="ui-btn-text" style="display:block;">'
								+ '</div></div></li>')
								.appendTo( menu )
							;
							
							if( jThis.css('display') == 'none' ) {
								elem.css('display','none');
								jThis.css('display','');
							}
							
							jThis
								.removeClass('ui-link')
								.addClass('ui-link-inherit')
							;
							elem.find('.ui-btn-text').append( jThis );
						});
						
						$.extend(menuPage[0], {
							item:item
						});
						
						$.extend(last[0], {
							menuPage:menuPage,
							screen:screen,
						});
						
						$.extend(screen.get(0),{
							menuPage:menuPage,
							screen:screen
						});
						
						screen.click("click", function( event ){
							var self = this;

							self.menuPage
								.addClass("ui-selectmenu-hidden")
								.css({
									top:''
									,left:''
								})
							;
							self.screen.addClass("ui-screen-hidden");
						});
						
						last.click(function(evt) {
							var self = this,
								jThis = $(this),
								menuHeight = menu.parent().outerHeight(),
								menuWidth = menu.parent().outerWidth(),
								scrollTop = $(window).scrollTop(),
								btnOffset = jThis.offset().top,
								screenHeight = window.innerHeight,
								screenWidth = window.innerWidth;
							
							var roomtop = btnOffset - scrollTop,
								roombot = scrollTop + screenHeight - btnOffset,
								halfheight = menuHeight / 2,
								maxwidth = parseFloat( menu.parent().css('max-width')),
								newtop, newleft;
							
							if( roomtop > menuHeight / 2 && roombot > menuHeight / 2 ){
								newtop = btnOffset + ( jThis.outerHeight() / 2 ) - halfheight;
							}
							else{
								//30px tolerance off the edges
								newtop = roomtop > roombot ? scrollTop + screenHeight - menuHeight - 30 : scrollTop + 30;
							}

							// if the menuwidth is smaller than the screen center is
							if (menuWidth < maxwidth) {
								newleft = (screenWidth - menuWidth) / 2;
							} else { //otherwise insure a >= 30px offset from the left
								newleft = jThis.offset().left + jThis.outerWidth() / 2 - menuWidth / 2;
								// 30px tolerance off the edges
								if (newleft < 30) {
									newleft = 30;
								} else if ((newleft + menuWidth) > screenWidth) {
									newleft = screenWidth - menuWidth - 30;
								}
							}
							
							self.menuPage
								.removeClass("ui-selectmenu-hidden")
								.css({
									top: newtop,
									left: newleft
								})
								.addClass("in");
							
							self.screen.removeClass("ui-screen-hidden");
						});

						//create the item
						last.appendTo(item)
							.addClass( "ui-li-link-alt" )
							.buttonMarkup({
								shadow: false,
								corners: false,
								theme: itemTheme,
								icon: false,
								iconpos: false
							})
							.find( ".ui-btn-inner" )
								.append(
									$( "<span />" ).buttonMarkup({
										shadow: true,
										corners: true,
										theme: splittheme,
										iconpos: "notext",
										icon: listspliticon || last.jqmData( "icon" ) || o.splitIcon
									})
								)
							;
					}
				} else if ( item.jqmData( "role" ) === "list-divider" ) {

					itemClass += " ui-li-divider ui-btn ui-bar-" + dividertheme;
					item.attr( "role", "heading" );

					//reset counter when a divider heading is encountered
					if ( counter ) {
						counter = 1;
					}

				} else {
					itemClass += " ui-li-static ui-body-" + itemTheme;
				}
			}

			if ( o.inset ) {
				if ( pos === 0 ) {
					itemClass += " ui-corner-top";

					item.add( item.find( ".ui-btn-inner" ) )
						.find( ".ui-li-link-alt" )
							.addClass( "ui-corner-tr" )
						.end()
						.find( ".ui-li-thumb" )
							.addClass( "ui-corner-tl" );

					if ( item.next().next().length ) {
						self._removeCorners( item.next() );
					}
				}

				if ( pos === li.length - 1 ) {
					itemClass += " ui-corner-bottom";

					item.add( item.find( ".ui-btn-inner" ) )
						.find( ".ui-li-link-alt" )
							.addClass( "ui-corner-br" )
						.end()
						.find( ".ui-li-thumb" )
							.addClass( "ui-corner-bl" );

					if ( item.prev().prev().length ) {
						self._removeCorners( item.prev() );
					} else if ( item.prev().length ) {
						self._removeCorners( item.prev(), "bottom" );
					}
				}
			}

			if ( counter && itemClass.indexOf( "ui-li-divider" ) < 0 ) {
				countParent = item.is( ".ui-li-static:first" ) ? item : item.find( ".ui-link-inherit" );

				countParent.addClass( "ui-li-jsnumbering" )
					.prepend( "<span class='ui-li-dec'>" + (counter++) + ". </span>" );
			}

			item.add( item.children( ".ui-btn-inner" ) ).addClass( itemClass );

			if ( !create ) {
				self._itemApply( $list, item );
			}
		}
	},

	//create a string for ID/subpage url creation
	_idStringEscape: function( str ) {
		return str.replace(/[^a-zA-Z0-9]/g, '-');
	},

	_createSubPages: function() {
		var parentList = this.element,
			parentPage = parentList.closest( ".ui-page" ),
			parentUrl = parentPage.jqmData( "url" ),
			parentId = parentUrl || parentPage[ 0 ][ $.expando ],
			parentListId = parentList.attr( "id" ),
			o = this.options,
			dns = "data-" + $.mobile.ns,
			self = this,
			persistentFooterID = parentPage.find( ":jqmData(role='footer')" ).jqmData( "id" ),
			hasSubPages;

		if ( typeof listCountPerPage[ parentId ] === "undefined" ) {
			listCountPerPage[ parentId ] = -1;
		}

		parentListId = parentListId || ++listCountPerPage[ parentId ];

		$( parentList.find( "li>ul, li>ol" ).toArray().reverse() ).each(function( i ) {
			var self = this,
				list = $( this ),
				listId = list.attr( "id" ) || parentListId + "-" + i,
				parent = list.parent(),
				nodeEls = $( list.prevAll().toArray().reverse() ),
				nodeEls = nodeEls.length ? nodeEls : $( "<span>" + $.trim(parent.contents()[ 0 ].nodeValue) + "</span>" ),
				title = nodeEls.first().text(),//url limits to first 30 chars of text
				id = ( parentUrl || "" ) + "&" + $.mobile.subPageUrlKey + "=" + listId,
				theme = list.jqmData( "theme" ) || o.theme,
				countTheme = list.jqmData( "counttheme" ) || parentList.jqmData( "counttheme" ) || o.countTheme,
				newPage, anchor;

			//define hasSubPages for use in later removal
			hasSubPages = true;

			newPage = list.detach()
						.wrap( "<div " + dns + "role='page' " +	dns + "url='" + id + "' " + dns + "theme='" + theme + "' " + dns + "count-theme='" + countTheme + "'><div " + dns + "role='content'></div></div>" )
						.parent()
							.before( "<div " + dns + "role='header' " + dns + "theme='" + o.headerTheme + "'><div class='ui-title'>" + title + "</div></div>" )
							.after( persistentFooterID ? $( "<div " + dns + "role='footer' " + dns + "id='"+ persistentFooterID +"'>") : "" )
							.parent()
								.appendTo( $.mobile.pageContainer );

			newPage.page();

			anchor = parent.find('a:first');

			if ( !anchor.length ) {
				anchor = $( "<a/>" ).html( nodeEls || title ).prependTo( parent.empty() );
			}

			anchor.attr( "href", "#" + id );

		}).listview();

		//on pagehide, remove any nested pages along with the parent page, as long as they aren't active
		if( hasSubPages && parentPage.data("page").options.domCache === false ){
			var newRemove = function( e, ui ){
				var nextPage = ui.nextPage, npURL;

				if( ui.nextPage ){
					npURL = nextPage.jqmData( "url" );
					if( npURL.indexOf( parentUrl + "&" + $.mobile.subPageUrlKey ) !== 0 ){
						self.childPages().remove();
						parentPage.remove();
					}
				}
			};

			// unbind the original page remove and replace with our specialized version
			parentPage
				.unbind( "pagehide.remove" )
				.bind( "pagehide.remove", newRemove);
		}
	},

	// TODO sort out a better way to track sub pages of the listview this is brittle
	childPages: function(){
		var parentUrl = this.parentPage.jqmData( "url" );

		return $( ":jqmData(url^='"+  parentUrl + "&" + $.mobile.subPageUrlKey +"')");
	}
});

//auto self-init widgets
$( document ).bind( "pagecreate create", function( e ){
	$( $.mobile.contextListview.prototype.options.initSelector, e.target ).contextListview();
});

})( jQuery );