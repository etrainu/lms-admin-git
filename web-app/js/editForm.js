$(function() {
	$('#editForm').bind('submit',function(event) {
		var $this = $(this);
		
		$.mobile.showPageLoadingMsg();
		$.ajax({
			url: $this.attr( "action" ),
			method: $this.attr( "method" ),
			dataType: 'JSON',
			data: $this.serialize(),
			success: function(resp) {
				$.mobile.hidePageLoadingMsg();
				
				//todo make this a bit more pretty...perhaps style it?
				$('<p></p>')
					.html( resp.message )
					.appendTo(
						$("<div></div>")
							.addClass("message")
							.prependTo(
								$(":jqmData(role='content')")	
							)
					)
				;
				window.scrollTo(0,0);
			}
		});
		
		event.preventDefault();
	});
});